# Contributor: Jesús E. <heckyel@hyperbola.info>

pkgname=emacs-yasnippet
_srcname=yasnippet
pkgver=0.14.0
pkgrel=1
pkgdesc='A template system for Emacs'
arch=('any')
url='https://github.com/joaotavora/yasnippet'
license=('GPL-3')
depends=('emacs')
makedepends=('ruby')
install='emacs-yasnippet.install'
source=("$pkgname-$pkgver.tar.gz::https://github.com/joaotavora/yasnippet/archive/$pkgver.tar.gz"
        '0001-clear-readme.patch')
sha512sums=('c7f704187b3c700f8ea9e0205941d7a32602b6994307007cb83aa984ccf7e7e37be9589277a56c294703eab82ee4b1dffae7040f2e175e7d56cda5ef3c70eb39'
            '8a346209fc1e4bde6dba0144ce0440b8b7d1d8bf7f34e1c64aa26c48698d3203ebe6acb7daac97dfa9aec4602083e0813bec7f8dee198ad44bb9f07fcd7785fb')

prepare() {
  cd "$_srcname-$pkgver"

  patch -p1 -i "${srcdir}/0001-clear-readme.patch"
}

build() {
  cd "$_srcname-$pkgver"

  rake compile && rake doc
}

package() {
  cd "$_srcname-$pkgver"

  install -d "$pkgdir/usr/share/emacs/site-lisp/yasnippet"
  for i in yasnippet.el{,c}; do
    install -m644 "$i" "$pkgdir/usr/share/emacs/site-lisp/yasnippet/$i"
  done

  # doc
  install -d "$pkgdir/usr/share/doc/$pkgname/"
  for f in doc/*.{html,org}; do
    install -m644 "$f" "$pkgdir/usr/share/doc/$pkgname/$(basename "$f")"
  done
  install -m644 README.mdown "$pkgdir/usr/share/doc/$pkgname/README.md"

  # license
  install -d "$pkgdir/usr/share/licenses/$pkgname"
  sed -n -e '3,26s/;; //' -e '3,26p' yasnippet.el > "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}

# vim:set ts=2 sw=2 et:
