#!/sbin/openrc-run
# Copyright 2018 Hyperbola Project
# Distributed under the terms of the GNU General Public License v2

getconf() {
	v=$(my_print_defaults --mysqld | grep ^--$1)
	[ -z $v ] && echo $2 || echo ${v#*=}
}

retry="60"
command="/usr/bin/mysqld_safe"
command_args="--syslog --nowatch"
command_user="mysql"

depend() {
	use logger net
	need localmount
}

start_pre() {
	pidfile=$(getconf pid-file "/run/mysqld/mysqld.pid")
	command_args="$command_args --pid-file=$pidfile"
	required_dirs=$(getconf datadir "/var/lib/mysql")
	if [ ! -d $required_dirs/mysql ]; then
		eerror "Datadir '$required_dirs' is empty or invalid."
		eerror "Run 'mysql_install_db --user=mysql --basedir=/usr --datadir=/var/lib/mysql' to create a new database."
	fi
	checkpath --directory --owner mysql:mysql --mode 0755 /run/mysqld
}

start_post() {
	ewaitfile 10 $(getconf socket "/run/mysqld/mysqld.sock")
}

stop_pre() {
	pidfile=$(getconf pid-file "/run/mysqld/mysqld.pid")
}
