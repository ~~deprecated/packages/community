# Maintainer (Arch): Johannes Löthberg <johannes@kyriasis.com>
# Contributor (Arch): Moritz Bunkus <moritz@bunkus.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=perl-json-any
pkgver=1.39
pkgrel=2.hyperbola1

pkgdesc="(DEPRECATED) Wrapper Class for the various JSON classes"
url='https://metacpan.org/release/JSON-Any'
arch=('any')
license=('GPL')

depends=('perl-json>=2.02' 'perl-json-xs>=2.3' 'perl-yaml-syck' 'perl-namespace-clean')
makedepends=('perl-test-fatal' 'perl-test-requires' 'perl-test-warnings>=0.009' 'perl-test-without-module')

source=("https://cpan.metacpan.org/authors/id/E/ET/ETHER/JSON-Any-$pkgver.tar.gz")

sha512sums=('a4e9494ef650fe6f0144fddad49962e717470390b5783ab7bed6ef1c34fa7aa3d4f8699b9967ec8ca8813f43ee8c1d594e5af4d4962929791aa95b470104cd9b')

options=('!emptydirs')

build() {
	cd JSON-Any-"$pkgver"

	# Setting these env variables overwrites any command-line-options we don't want...
	export PERL_MM_USE_DEFAULT=1 PERL_AUTOINSTALL=--skipdeps \
	       PERL_MM_OPT="" PERL_MB_OPT="" MODULEBUILDRC=/dev/null

	perl Makefile.PL INSTALLDIRS=vendor
	make
}

check() {
	cd JSON-Any-"$pkgver"
	make test
}

package() {
	cd JSON-Any-"$pkgver"
	make install DESTDIR="$pkgdir"
	find "$pkgdir" -name .packlist -o -name perllocal.pod -delete
}
