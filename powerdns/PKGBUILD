# Maintainer (Arch): Levente Polyak <anthraxx[at]archlinux[dot]org>
# Contributor (Arch): Alexander Rødseth <rodseth@gmail.com>
# Contributor (Arch): Jan de Groot <jgc@archlinux.org>
# Contributor (Arch): Kevin Mihelich <kevin@archlinuxarm.org>
# Contributor (Arch): Remi Gacogne <rgacogne[at]archlinux[dot]org>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=powerdns
pkgver=4.0.3
_debver=4.0.3
_debrel=1+deb9u3
pkgrel=3.hyperbola2
pkgdesc='Authoritative DNS server, with libressl and OpenRC support'
url='https://www.powerdns.com/'
arch=('i686' 'x86_64')
license=('GPL-2')
depends=('boost-libs' 'libsodium' 'lua' 'libressl' 'protobuf' 'sqlite')
makedepends=('boost' 'setconf' 'libmariadbclient' 'postgresql-libs' 'lua' 'libldap'
             'sqlite' 'geoip' 'yaml-cpp' 'pkg-config' 'quilt')
optdepends=('libmariadbclient: MariaDB/MySQL backend'
            'postgresql-libs: PostgreSQL backend'
            'libldap: LDAP backend'
            'geoip: GeoIP backend'
            'yaml-cpp: GeoIP backend')
provides=('pdns')
conflicts=('pdns')
backup=('etc/powerdns/pdns.conf')
source=(https://downloads.powerdns.com/releases/pdns-${pkgver}.tar.bz2{,.asc}
        https://deb.debian.org/debian/pool/main/p/pdns/pdns_$_debver-$_debrel.debian.tar.xz
        fix-${pkgver}-tests.patch
        pdns.initd
        libressl-2.7.patch)
sha512sums=('58d33ac6cf457a916bae6abd8d2dc17f76fbcd1bd9e649948584dd669f5596b43e3e4d91841700ea1ea2cd1ac102749e503cd9075273540f33a2321e20d8bfc2'
            'SKIP'
            'a073972172b1ea5d5f4a23792eb347299baafe29f5aa4a57077c215b77e468f46144bfbf1fb51858707389a40e610a13983996b1fc428629d4b90d1ba4d022a1'
            '9f6b3d794495c21280f1077b644dd6ecc10112823806c9a4db9b0819445e6450eb1a874c95199e918b7a1ad09e83192cc09d3ecf02d11b7da7488e6e03c3810a'
            '6392f59f82b82621b6cab6823f7761cd875bc6818c48db846df289057c02d8119a1024c5ecdf70a5ca8a55c00a2c3c56240d36b42cc038a25e8616ab9b966dd6'
            '6e53dc015a1e5fac9e8264157de81f36826b1f23901a524b9b873a321ae86c55fda55706e54f478c7567fc0d51f4083c8652e5665cd5a91663ce6e997e76a5ed')
validpgpkeys=('B76CD4671C0968BAA87DE61C5E50715BF2FFE1A7') # Pieter Lexis <pieter.lexis@powerdns.com>

prepare() {
  cd pdns-${pkgver}

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  patch -p1 < "${srcdir}/fix-${pkgver}-tests.patch"

  # Fix compilation with LibreSSL 2.7.0+
  patch -p1 -i "${srcdir}/libressl-2.7.patch"

  # Default settings
  for keyvalue in \
    allow-recursion=127.0.0.1 \
    chroot=/var/empty \
    config-dir=/etc/powerdns \
    daemon=no \
    distributor-threads=3 \
    guardian=no \
    local-port=53 \
    loglevel=3 \
    module-dir=/usr/lib/powerdns \
    setgid=nobody \
    setuid=nobody \
    socket-dir=/var/run \
    webserver=no
  do
    setconf -a pdns.conf "${keyvalue}"
  done

  # Using simple so we can bind it to a chroot
  sed 's|notify|simple|' -i pdns/pdns.service.in
}

build() {
  cd pdns-${pkgver}
  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc/powerdns \
    --mandir=/usr/share/man \
    --with-modules='' \
    --with-dynmodules="bind gmysql geoip gpgsql gsqlite3 ldap lua pipe random remote" \
    --docdir=/usr/share/doc/powerdns \
    --with-sqlite3 \
    --enable-libsodium \
    --enable-tools \
    --disable-dependency-tracking \
    --disable-silent-rules \
    --enable-reproducible \
    --enable-unit-tests
  make
}

check() {
  make -C pdns-${pkgver} check
}

package() {
  cd pdns-${pkgver}
  make DESTDIR="${pkgdir}" install
  # apply defaults to generated pdns.conf
  mv "${pkgdir}/etc/powerdns/pdns.conf"{-dist,}
  cat pdns.conf >> "${pkgdir}/etc/powerdns/pdns.conf"
  # adjust modules dir to match pkgname
  mv "${pkgdir}/usr/lib/"{pdns,powerdns}
  # install OpenRC
  install -Dm755 "${srcdir}/pdns.initd" "${pkgdir}/etc/init.d/pdns"
  # install license
  install -Dm644 COPYING "${pkgdir}/usr/share/licenses/powerdns/COPYING"
}

# vim: ts=2 sw=2 et:
