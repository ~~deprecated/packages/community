# Maintainer (Arch): Lukas Fleischer <lfleischer@archlinux.org>
# Contributor (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): Andreas Wagner <a.wagner@stud.uni-frankfurt.de>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=lockdev
pkgver=1.0.3_1.6
_pkgver=1.0.3
pkgrel=4.hyperbola4
pkgdesc='Run-time shared library for locking devices, using _both_ FSSTND and SVr4 methods, without systemd support'
url='https://packages.qa.debian.org/l/lockdev.html'
license=("LGPL-2.1")
arch=('i686' 'x86_64')
depends=('glibc')
install=lockdev.install
source=("https://deb.debian.org/debian/pool/main/l/${pkgname}/${pkgname}_${_pkgver}.orig.tar.gz"
        'build.patch'
        'gcc-4.7.patch')
sha512sums=('4066985059add674ef8a9195e611db8ead89ca6191c04b7f29243b12a3448c82a3d0ea852bf99e26e1c71f163891acd198867650a5c4843f787e1944a8bc7f2b'
            '79ff3c22fbf38966310de99eefbc897c4f78b05c42d830ef3fc5ecb4234d81e58655bb2ab4564d7da0dbe04348ec496468b840a9992abe6b95b8b423916430ae'
            'c97ea9e3974d4eccc30472605a24f4393c60ada66ae5a0ad30502e3f939fb191a2772686241f0d12e417b33350edc015ec2d0238873031773ed6c39dbace0429')

prepare() {
  cd "${pkgname}-${_pkgver}"
  sed -i "s|CFLAGS	= -g|CFLAGS	= -g -fPIC|" Makefile
  patch -p1 -i ../build.patch # FS#33280
  patch -p1 -i ../gcc-4.7.patch # FS#34457
}

build() {
  cd "${pkgname}-${_pkgver}"
  make CFLAGS="${CFLAGS} -fPIC"
}

package() {
  cd "${pkgname}-${_pkgver}"
  make basedir="${pkgdir}/usr" install
  install -Dm644 LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
}
