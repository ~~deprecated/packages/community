# Maintainer (Arch): Jiachen Yang <farseerfc@gmail.com>
# Maintainer (AUR): Vlad M. <vlad@archlinux.net>
# Contributor (Arch): Håvard Pettersson <mail@haavard.me>
# Contributor (Arch): Kevin MacMartin <prurigro at gmail dot com>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=qtox
_pkgname=qTox
pkgver=1.16.3
pkgrel=2.hyperbola1.backports1
pkgdesc='Powerful Tox client written in C++/Qt that follows the Tox design guidelines'
arch=('i686' 'x86_64')
url='https://github.com/tux3/qTox'
license=('GPL3')
depends=('desktop-file-utils'
         'libxss'
         'openal'
         'gtk2'
         'ffmpeg'
         'qrencode'
         'qt5-svg'
         'sqlcipher'
         'libexif'
         'toxcore'
         'mesa-libgl')
makedepends=('qt5-tools' 'check' 'cmake')

source=("https://github.com/$_pkgname/$_pkgname/releases/download/v$pkgver/v$pkgver.tar.lz"{,.asc})
sha512sums=('97c9becd6a90fc6a09a24fb52c1863dd880d19e9a6807499e875020a4012259f926014d98a4c831fd534028d9bcfcb76c5bb32001a553c22e2174a992a493a65'
            'SKIP')

# GPG key fingerprints taken from https://github.com/qTox/qTox/blob/$_gitver/README.md
validpgpkeys=('DA262CC93C0E1E525AD21C8596775D454B8EBF44'  # sudden6 <sudden6@gmx.at>
              'BA7883E22F9D35945BA3376053137C3033F09008'  # Zetok Zalbavar <zetok@openmailbox.org>
              '1157616BBD860C539926F8139591A163FF9BE04C'  # - antis81
              '31039166FA902CA50D05D6085AF9F2E29107C727'  # - Diadlo
              'C7A2552D0B250F983827742C133203A3AC399151'  # - initramfs
              '2880C860D95C909D3DA45C687E086DD661263264'  # - tux3
              '74ADB7A7F8ECC1009916C4E42AA8E0DA1B31FB3C'  # - AnthonyBilinski subkey
              '7EB339FE881747E701B7D472EBE36E66A8429B99'  # - AnthonyBilinski
              'CA9221C5389B7C50AA5F779352A50775BE13DF17'  # - noavarice
              '141C880E8BA25B198D0F850F7C132143C1A3A7D4'  # – tox-user
             )

prepare() {
  # disable -Werror for release packaging
  sed -e 's|-Werror||' -i CMakeLists.txt
}

check() {
  cd build
  make test
}

build() {
  mkdir -p build
  cd build
  cmake .. -DCMAKE_INSTALL_PREFIX=/usr
  make
}

package() {
  cd build
  make DESTDIR="$pkgdir" install
}
