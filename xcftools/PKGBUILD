# Maintainer: Márcio Silva <coadde@hyperbola.info>

pkgname=xcftools
pkgver=1.0.7
pkgrel=1
_debver=$pkgver
_debrev=
_debrel=6
pkgdesc='A set of command line tools for extracting information from the XCF (Gimp native file format)'
url=http://henning.makholm.net/software
arch=(i686 x86_64)
license=(GPL2)
depends=(libpng perl)
makedepends=(quilt)
optdepends=('gimp: for edit xcf files')
source=(http://henning.makholm.net/$pkgname/$pkgname-$pkgver.tar.gz{,.asc} # no https websites found
        https://deb.debian.org/debian/pool/main/${pkgname::1}/$pkgname/${pkgname}_$_debver$_debrev-$_debrel.debian.tar.xz)
sha512sums=(88af4791e18650562db259bd96bfd122364c21b7ea7a5692d4679e619667bdbcc179040a1d912c8fd623bc2d2735461da237ccde4646553b15d6072cc4493203
            SKIP
            63962f05fb0f1b38a7adc8ecef06b173b682ceefe52bae2b1b8b7280c6751b0cfd028c7b916983cb1e264fa3ef4245a5182ed1a4ce65a3513f48a8f5c6dea47d)
validpgpkeys=(
              12C24D5809950EEFCBC91E88F8DAD7195CBC1773 # Henning Makholm <henning@debian.org>
)

prepare() {
  cd $pkgname-$pkgver

  if [ ${pkgver::4} = ${_debver::4} ]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS=--no-timestamps

    mv ../debian .

    quilt push -av
  fi
}

build() {
  cd $pkgname-$pkgver

  ./configure --prefix=/usr
  make
}

package() {
  cd $pkgname-$pkgver

  make DESTDIR=$pkgdir install
}

# vim:set ts=2 sw=2 et:
