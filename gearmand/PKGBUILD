# Maintainer (Arch): Massimiliano Torromeo <massimiliano.torromeo@gmail.com>
# Contributor (Arch): Lev Lybin <lev.lybin@gmail.com>
# Contributor (Arch): Vadym Abramchuk <abramm@gmail.com>
# Contributor (Arch): John Gerritse <reaphsharc@gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

pkgname=gearmand
pkgver=1.1.18
_debver=1.1.18+ds
_debrel=3
pkgrel=7.hyperbola2
pkgdesc="Distributed job queue server, with OpenRC support"
arch=('i686' 'x86_64')
depends=('libevent' 'boost-libs' 'libmariadbclient' 'postgresql-libs' 'sqlite' 'hiredis')
makedepends=('boost' 'gperf' 'python-sphinx' 'quilt')
conflicts=('gearman')
replaces=('gearman')
provides=("gearman=$pkgver")
url="http://gearman.org/"
license=('Modified-BSD')
backup=('etc/gearmand.conf'
        'etc/conf.d/gearmand')
source=(https://github.com/gearman/$pkgname/releases/download/$pkgver/$pkgname-$pkgver.tar.gz
        https://deb.debian.org/debian/pool/main/g/gearmand/gearmand_$_debver-$_debrel.debian.tar.xz
        gearmand.conf
        gearmand.confd
        gearmand.initd)
sha512sums=('fd2c978775bde19a8f1ffaf720b4c8adfda9859e5f554b247e7edca15fcc684168fb279af6c63e29c0524c8c863a9f3d07ea802e67eec42be793c0487b9beb9a'
            'f995b764a71134c8d8f23d23d5d8fac4898b05f439e00fb2851a36ed4555670b8b56ff28e08907fc3f12d592c5f8c7c910ea88ef0a02f47a0d170aa40e34ab00'
            'a3172f220211dcb8160f088d1555f4604267b99d22a4279e2a5a3df9b2f4a19517b294e1f9eb081b33d35bb0991c7c229f0d3be8a2a752d547d1e118d75e18b0'
            '7edfb51212727b933cf5ca46a8ac72db2fe3f49e04bc856a69eeb77874e35b160ba85844b794f944d3b308624d6c51126bbb721a53b64b1d017a57ed43f9423b'
            'af31acdb8e028eadac96add91b14abebfc2f3513c19dd5b1c73f8b0db6f0bb37189d6f401b50cfc304994af70149af3e113f30250b4485637652380834988b42')

prepare() {
  cd "$srcdir"/$pkgname-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/0003-Parse-upstream-version-from-debian-changelog-and-not.patch || true

    quilt push -av
  fi

  # parse upstream version
   sed -i "s|m4_esyscmd_s[(][[]git[ ]describe[ ]--always[]][)]|[$pkgver]|" version.m4

   autoreconf -vfi
}

build() {
  export LDFLAGS="$LDFLAGS -lboost_system"

  cd "$srcdir"/$pkgname-$pkgver

  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --localstatedir=/run \
    --disable-static \
    --enable-ssl
  make
}

package() {
  cd "$srcdir"/$pkgname-$pkgver

  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING "$pkgdir"/usr/share/licenses/$pkgname/COPYING

  cd "$srcdir"

  install -Dm644 gearmand.conf "$pkgdir"/etc/gearmand.conf
  install -Dm644 gearmand.confd "$pkgdir"/etc/conf.d/gearmand
  install -Dm755 gearmand.initd "$pkgdir"/etc/init.d/gearmand
}
