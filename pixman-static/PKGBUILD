# Maintainer: Márcio Silva <coadde@hyperbola.info>
# Maintainer: André Silva <emulatorman@hyperbola.info>

# Based on pixman package

_pkgname=pixman
pkgname=pixman-static
_openbsdver=6.5
pkgver=0.36.0
pkgrel=2.backports1
pkgdesc="The pixel-manipulation library for X and cairo, provided by Xenocara (static libraries only)"
arch=(i686 x86_64)
url="https://www.xenocara.org"
license=('Expat')
depends=('pixman')
makedepends=('xenocara-util-macros')
options=('staticlibs')
source=(https://repo.hyperbola.info:50000/sources/xenocara-libre/$_openbsdver/lib/pixman-$pkgver.tar.xz{,.sig})
sha512sums=('28cf30c3f3e0dbbc494046fb4571422e66c8bf1c6782aa911a54c51a38ee7ed0563850aa9561c8c5df7c2fdc20978b92e4e714d183d0506c5adaf4753e23205d'
            'SKIP')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

prepare() {
  cd "xenocara-$_openbsdver/lib/pixman"
  autoreconf -vfi
}

build() {
  cd "xenocara-$_openbsdver/lib/pixman"
  ./configure --prefix=/usr --enable-static
  make
}

check() {
  cd "xenocara-$_openbsdver/lib/pixman"
  make check
}

package() {
  cd "xenocara-$_openbsdver/lib/pixman"
  make DESTDIR="$pkgdir" install

  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"

  # remove conflicting files
  rm -vr $pkgdir/usr/{include,lib/pkgconfig}
  rm -v $pkgdir/usr/lib/lib*.so*
}
