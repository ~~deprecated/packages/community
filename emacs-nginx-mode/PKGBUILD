# Contributor: Jesús E. <heckyel@hyperbola.info>

pkgname=emacs-nginx-mode
_srcname=nginx-mode
pkgver=1.1.9
pkgrel=1
pkgdesc='Emacs editing mode for Nginx config files'
arch=('any')
url='https://github.com/ajc/nginx-mode'
license=('GPL-2')
depends=('emacs')
install='emacs-nginx-mode.install'
source=("$pkgname-$pkgver.tar.gz::https://github.com/ajc/nginx-mode/archive/v$pkgver.tar.gz"
        '0001-clear-README.patch')
sha512sums=('a209b83f39696ccea8d45b0d90a25b4cdb56f85f1288ca9a7bf83b24a7d69ae2a1caea5e71f7c62ef693f8aac3e74e5dce7761b69ded7da037904df1c9d43536'
            'e4961ba18366d0b96700deb84472bc4bccdf2f93b94729070134c5864d65aa4f3951df4ee9efe075f389a310f02dfb7ec71fbc433fd06052a6acea21810a8d98')

prepare() {
  cd "$_srcname-$pkgver"

  patch -p1 -i "${srcdir}/0001-clear-README.patch"
}

build() {
  cd "$_srcname-$pkgver"

  emacs -q --no-splash -batch -L . -f batch-byte-compile nginx-mode.el
}

package() {
  cd "$_srcname-$pkgver"

  install -d "$pkgdir/usr/share/emacs/site-lisp/nginx-mode"
  for i in nginx-mode.el{,c}; do
    install -m644 "$i" "$pkgdir/usr/share/emacs/site-lisp/nginx-mode/$i"
  done

  # license
  install -d "$pkgdir/usr/share/licenses/$pkgname"
  sed -n -e '3,24s/;; //' -e '3,24p' nginx-mode.el > "$pkgdir/usr/share/licenses/$pkgname/COPYING"

}
# vim:set ts=2 sw=2 et:
