# Maintainer (Arch): Kyle Keen <keenerd@gmail.com>
# Contributor (Arch): Daniel J Griffiths <ghost1227@archlinux.us>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=macchanger
pkgver=1.7.0
_debver=1.7.0
_debrel=5.3
pkgrel=2.hyperbola1
pkgdesc="A small utility to change your NIC's MAC address"
arch=('i686' 'x86_64')
url="https://github.com/alobbs/macchanger"
license=('GPL-3')
depends=('glibc')
makedepends=('quilt')
source=("https://github.com/alobbs/macchanger/archive/$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/m/macchanger/macchanger_$_debver-$_debrel.debian.tar.xz")
sha512sums=('55fbfe4f4e94f0822e7a71b8c7052ed6c3a05066859044498bd68d068c33902b5718e92c8e00b73af71b57ce825644e1f81eb4dd7ef11701f197b953d7853aff'
            'd9ca9ef51b5e236452537a64c069f2407bd2116adb8d3a665736e71137871ff7916e23e01e5bea2cd69cbcfdfa59ac26747cc9669c5d84bab560b1577e077778')

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  ./autogen.sh
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  ./configure --prefix=/usr \
              --bindir=/sbin \
              --mandir=/usr/share/man \
              --infodir=/usr/share/info
  make
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}" install
  install -Dm644 COPYING "${pkgdir}/usr/share/licenses/${pkgname}/COPYING"
}
