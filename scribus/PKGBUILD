# Maintainer (Arch): Gaetan Bisson <bisson@archlinux.org>
# Contributor (Arch): Daniel J Griffiths <ghost1227@archlinux.us>
# Contributor (Arch): Ronald van Haren <ronald.archlinux.org>
# Contributor (Arch): tobias <tobias@archlinux.org>
# Contributor (Arch): Ben <ben@benmazer.net>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=scribus
pkgver=1.4.6
_debver=1.4.6+dfsg
_debrel=4
pkgrel=4.hyperbola1
pkgdesc='Desktop publishing software'
url='https://www.scribus.net/'
license=('GPL-2')
arch=('i686' 'x86_64')
makedepends=('cmake' 'boost' 'mesa' 'quilt')
optdepends=('tk: scripts based on tkinter')
depends=('qt4' 'cairo' 'lcms2' 'libcups' 'libxml2' 'hyphen'
         'ghostscript' 'python2' 'podofo' 'boost-libs'
         'desktop-file-utils' 'shared-mime-info')
validpgpkeys=('5086B8D68E70FDDF4C40045AEF7B95E7F60166DA')
source=("https://downloads.sourceforge.net/${pkgname}/${pkgname}-${pkgver}.tar.xz"{,.asc}
        "https://deb.debian.org/debian/pool/main/s/scribus/scribus_$_debver-$_debrel.debian.tar.xz")
sha512sums=('74d8a89eb767535bcd8fe5e3c55d03709d59ff8fc5280005bcc2ad36cae1d37c8442ab85abaea86fdee9f351a901c86947231001324e2d8df00cbd8e5c18d1e3'
            'SKIP'
            'fa60aa7ca6393070a50a6025a8b808b9aed10675174811b4c2c66bde562ea37403f668888a5d99c9cb1651f4fd3105f609f5e589fc051ec0bfcbf2273233a48f')

prepare() {
	cd "${srcdir}/${pkgname}-${pkgver}"
	if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
		# Debian patches
		export QUILT_PATCHES=debian/patches
		export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
		export QUILT_DIFF_ARGS='--no-timestamps'

		mv "$srcdir"/debian .

		quilt push -av
	fi
	sed \
		-e 's|#!/usr/bin/python|#!/usr/bin/python2|' \
		-e 's|#!/usr/bin/env python|#!/usr/bin/env python2|' \
		-i scribus/plugins/scriptplugin/{samples,scripts}/*
	install -d build
}

build() {
	cd "${srcdir}/${pkgname}-${pkgver}/build"
	cmake .. \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_SKIP_RPATH=ON \
		-DLIB_SUFFIX=""
	make
}

package() {
	cd "${srcdir}/${pkgname}-${pkgver}/build"
	make DESTDIR="${pkgdir}" install
	install -Dm644 ../COPYING "${pkgdir}/usr/share/licenses/${pkgname}/COPYING"
	install -Dm644 ../scribus.desktop "${pkgdir}/usr/share/applications/scribus.desktop"
	install -Dm644 ../resources/icons/AppIcon.png "${pkgdir}/usr/share/icons/hicolor/32x32/apps/scribus.png"
}
