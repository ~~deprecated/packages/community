# Maintainer (Arch): Balló György <ballogyor+arch at gmail dot com>
# Contributor (Arch): Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor (Arch): AndyRTR <andyrtr@archlinux.org>
# Contributor (Arch): kiefer <jorgelmadrid@gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgbase=lxdm
pkgname=(lxdm lxdm-gtk3)
pkgver=0.5.3
_debver=0.5.3
_debrel=2
pkgrel=4.hyperbola4
pkgdesc='Lightweight X11 Display Manager, with OpenRC support'
arch=('i686' 'x86_64')
url="https://lxde.org/"
license=('GPL-3')
depends=('gtk2' 'gtk3' 'xenocara-server')
makedepends=('intltool' 'iso-codes' 'quilt')
optdepends=('iso-codes: show language names in language chooser'
            'librsvg: display the default background')
install=$pkgbase.install
backup=('etc/lxdm/lxdm.conf' 'etc/pam.d/lxdm' 'etc/lxdm/Xsession'
        'etc/lxdm/PreLogin' 'etc/lxdm/LoginReady' 'etc/lxdm/PostLogin'
        'etc/lxdm/PostLogout' 'etc/lxdm/PreReboot' 'etc/lxdm/PreShutdown')
source=(https://downloads.sourceforge.net/lxde/$pkgbase-$pkgver.tar.xz
        https://deb.debian.org/debian/pool/main/l/lxdm/lxdm_$_debver-$_debrel.debian.tar.xz
        git-fixes.patch
        default-config.patch
        lxdm.pam
        Xsession
        lxdm.initd)
sha512sums=('0e3539c595a71cb850c1756dd075b8d477a4e111a4167714057cac06650e9587f338f797f14122e7b2a2f4ad35b38cd7172b83e996758abeeaf0146d65bbc26f'
            '2905e4b710e81fe50da234c6141a1e64ac418f47cccb92c770f30e0f9f502ff04cdb774628abd932aa0367bc3f9bc79a9ade52df2eeec69c7b84c389869bbcbd'
            '3ac6601dafe36cfe95a7895b3e138d184b2098a58b2e93fbb168e91cd51ffef5cbc55403138b326f68b85f8d4bb3c303dc734378dca808432edf5cf85e93afd5'
            'ef7db34dc027a27d4e3bc0214767d190d6e8fa41abe5b67e66a99171c383715e4fa61a456dcb04ccc714a27cdbd3e865ae07ed3a79b18ece148c3d5ff9847cb3'
            'cb912013a294f0801b357a43f3e5313ffa9ac5fcc493b2318843983388eb0b839c84060a97c355e12ca03f3b056644aa4a2bb8a74ed73a0f2405816b8d6efdc0'
            '1ab102ae5bbb5cf18e8bf55ca5ab3fee913efef6406c9e0dd81c3a8e5116baa29eafc428f556e29966f72d46d59f115f9c47aed0ae4d9141f2f79cea5e7761e1'
            '641ea9c9623838b83ccbc86f51f3c4ec1b43fde16d05791697ef6781314e9307365265d8b9b19a037b067f8d6600b7f77e4139f5854e252f34519243a5c9c9e2')

prepare(){
  cd $pkgbase-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/23_debian-config.patch || true
    rm -v debian/patches/42-fix-user-list.patch || true

    quilt push -av
  fi

  # Apply fixes from git
  patch -Np1 -i ../git-fixes.patch

  # Adjust Arch-specific settings
  patch -Np1 -i ../default-config.patch

  # Use our custom pam and Xsession files
  cp ../lxdm.pam pam/lxdm
  cp ../Xsession data/Xsession
}

build() {
  # GTK+ 2 version
  [ -d gtk2 ] || cp -r $pkgbase-$pkgver gtk2
  cd gtk2
  ./configure --prefix=/usr --libexecdir=/usr/libexec/lxdm \
              --sysconfdir=/etc --localstatedir=/var
  make

  cd "$srcdir"
  # GTK+ 3 version
  [ -d gtk3 ] || cp -r $pkgbase-$pkgver gtk3
  cd gtk3
  ./configure --prefix=/usr --libexecdir=/usr/libexec/lxdm \
              --sysconfdir=/etc --localstatedir=/var --enable-gtk3
  make
}

package_lxdm() {
  groups=('lxde')
  depends=('gtk2' 'xenocara-server')
  optdepends+=('gnome-themes-standard: default GTK+ theme')

  cd gtk2
  make DESTDIR="$pkgdir" install
  chmod 644 "$pkgdir/etc/lxdm/lxdm.conf"

  # License
  install -Dm644 COPYING "$pkgdir"/usr/share/licenses/$pkgname/COPYING

  # OpenRC
  install -Dm755 "$srcdir"/lxdm.initd "$pkgdir"/etc/init.d/lxdm
}

package_lxdm-gtk3() {
  groups=('lxde-gtk3')
  pkgdesc+=' (GTK+ 3 version)'
  depends=('gtk3' 'xenocara-server')
  conflicts=('lxdm')

  cd gtk3
  make DESTDIR="$pkgdir" install
  chmod 644 "$pkgdir/etc/lxdm/lxdm.conf"

  # License
  install -Dm644 COPYING "$pkgdir"/usr/share/licenses/$pkgname/COPYING

  # OpenRC
  install -Dm755 "$srcdir"/lxdm.initd "$pkgdir"/etc/init.d/lxdm
}
