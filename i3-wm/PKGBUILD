# Maintainer (Arch): Thorsten Töpper <atsutane-tu@freethoughts.de>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=i3-wm
_pkgsourcename=i3
pkgver=4.13
_debver=4.13
_debrel=1+deb9u1
pkgrel=1.hyperbola1
pkgdesc='An improved dynamic tiling window manager'
arch=('i686' 'x86_64')
url='https://i3wm.org/'
license=('Modified-BSD')
replaces=('i3' 'i3bar')
groups=('i3')
depends=('xcb-util-cursor' 'xcb-util-keysyms' 'xcb-util-wm' 'xcb-util-xrm'
         'libev' 'yajl' 'startup-notification' 'pango' 'libxkbcommon-x11')
makedepends=('bison' 'flex' 'quilt')
optdepends=('dmenu: As menu.'
            'i3lock: For locking your screen.'
            'i3status: To display systeminformation with a bar.'
            'perl: i3-save-tree and i3-dmenu-desktop'
            'perl-anyevent-i3: Features like saving the layout.'
            'perl-json-xs: Features like saving the layout.')
options=('docs' '!strip')
source=("https://i3wm.org/downloads/${_pkgsourcename}-${pkgver}.tar.bz2"
        "https://i3wm.org/downloads/${_pkgsourcename}-${pkgver}.tar.bz2.asc"
        "https://deb.debian.org/debian/pool/main/i/i3-wm/i3-wm_$_debver-$_debrel.debian.tar.xz")
sha512sums=('1bb1044e8d86e78d3ccb79d49f0eb26665dcd05a348058a5e57138151d74f57d77830efc3025893170fe1b8ec612f739f75247a427410f96286b09afd2c5f14c'
            'SKIP'
            '69984ddd7f546346614d74ed34aab7389c1ec619e9fb54c4e5b8f6c76263ac46f2d93d0b5581ca78f742f5fcb91b491e336187a6d56396d9a898bd3c128185ef')
validpgpkeys=('424E14D703E7C6D43D9D6F364E7160ED4AC8EE1D') # Michael Stapelberg

prepare() {
  cd "$srcdir/$_pkgsourcename-$pkgver"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "$srcdir/$_pkgsourcename-$pkgver"

  ./configure --prefix=/usr --sysconfdir=/etc
  cd ${CARCH}-pc-linux-gnu
  # In order to avoid problems with bison use only a single process
  MAKEFLAGS="-j1"
  make CPPFLAGS+="-U_FORTIFY_SOURCE"
}

package() {
  cd "$srcdir/$_pkgsourcename-$pkgver/${CARCH}-pc-linux-gnu"
  make DESTDIR="$pkgdir/" install

  mkdir -p "${pkgdir}/usr/share/man/man1/"
  install -m644 ../man/*.1 "${pkgdir}/usr/share/man/man1/"

  install -Dm644 ../LICENSE \
    ${pkgdir}/usr/share/licenses/${pkgname}/LICENSE
}

# vim:set ts=2 sw=2 et:
