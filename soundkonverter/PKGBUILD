# $Id: PKGBUILD 195998 2016-11-15 19:06:33Z arojas $
# Maintainer (Arch): Lukas Jirkovsky <l.jirkovsky@gmail.com>
# Contributor (Arch): Mateusz Herych <heniekk@gmail.com>
# Contributor (Arch): Eric Belanger <eric@archlinux.org>
# Contributor (Arch): Darwin Bautista <djclue917@gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=soundkonverter
pkgver=2.2.2
_debver=2.2.2
_debrel=1
pkgrel=2.hyperbola1
pkgdesc="Front-end to various audio converters, without nonfree faac and mac recommendation"
arch=('i686' 'x86_64' 'armv7h')
url="https://github.com/dfaust/soundkonverter/wiki"
license=('GPL')
depends=('kdebase-runtime' 'libkcddb4' 'cdparanoia' 'taglib')
makedepends=('automoc4' 'cmake' 'quilt')
optdepends=('cdrkit: cdda2wav backend'
            'faad2: faad backend'
            'ffmpeg: ffmpeg backend'
            'flac: flac backend'
            'fluidsynth: flouidsynth backend'
            'lame: lame backend'
            'mplayer: mplayer backend'
            'mp3gain: mp3gain backend'
            'opus-tools: opus backend'
            'speex: speexenc, speexdec backends'
            'timidity++: midi backend'
            'twolame: twolame backend'
            'vorbisgain: vorbisgain backend'
            'vorbis-tools: vorbis tools backend'
            'wavpack: wavpack backend'
            'sox: sox plugin (change sample rate, various effects)')
source=("https://github.com/dfaust/soundkonverter/archive/v$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/s/soundkonverter/soundkonverter_$_debver-$_debrel.debian.tar.xz")
sha512sums=('1d646759725645dd8eb8c2f486a64a57bfe17622e935f54f53630956da9a9a715d87ea1364143f1e8704ce4df0910d9c01bcb20885d8f5eafaef52b296679854'
            '4e8197b48da8296a19d6e5abf2a18659c8ceb6759d67d8e7db098957894d3c9a36470936cca0d0ec0dda13c2c989591de1af9b9744439cbb4afa4f61920bafaa')

prepare() {
  cd "$srcdir"/$pkgname-$pkgver
  if [[ $pkgver = $_debver ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "$srcdir"/$pkgname-$pkgver/src
  cmake . \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_BUILD_TYPE=Release
  make
}

package() {
  cd "$srcdir"/$pkgname-$pkgver/src
  make DESTDIR="$pkgdir" install
}
