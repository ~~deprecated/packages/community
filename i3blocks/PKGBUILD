# Maintainer (Arch): Christian Rebischke <Chris.Rebischke@archlinux.org>
# Contributor (Arch): Vlad M. <vlad@archlinux.net>
# Contributor (Arch): Patrice Peterson <runiq at archlinux dot us>
# Contributor (Arch): Vivien Didelot <vivien+aur@didelot.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=i3blocks
pkgver=1.4
_debver=1.4
_debrel=3
pkgrel=4.hyperbola1.1
pkgdesc='Define blocks for your i3bar status line, with pandoc support'
arch=('i686' 'x86_64')
groups=('i3')
url="https://github.com/vivien/i3blocks"
license=('GPL-3')
makedepends=('pandoc' 'quilt')
optdepends=('acpi: For battery script'
            'bc: For bandwidth script'
            'lm_sensors: For temperature script'
            'openvpn: For openvpn script'
            'playerctl: For mediaplayer script'
            'sysstat: For cpu_usage script')
backup=('etc/i3blocks.conf')
source=("https://github.com/vivien/${pkgname}/releases/download/${pkgver}/${pkgname}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/i/i3blocks/i3blocks_$_debver-$_debrel.debian.tar.xz"
        'fix-scripts.patch')
sha512sums=('c1f5abe90426e9f6284ef74001c9f50d08ce596b7bb8a284232116774375fa50168567785999266c8774fa39a1243c73f699a3e3be4440fbe16f33f0fde5cded'
            'b3d717cba243c5aab7dbac4bf0ec17517fb9edcd6082d5f6b1227b8b8a2136713f6dadd772884373f72b41d65b5e7e50b1a0fa642af935b7df81c01bf6c32f5b'
            'df6e2a80df53c80a79b0a784b2168dcc7dfef6fc4c34027b8c953c6f4d873dd45b6cb8ca2837b78dd9f4bdc480ed206c71a1bcf8e4e88e70aec0718fe2851bd3')

prepare() {
  cd "${pkgname}-${pkgver}"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  patch -Np1 -i "$srcdir/fix-scripts.patch"
}

build() {
  cd "${pkgname}-${pkgver}"
  make VERSION="${pkgver}" PREFIX=/usr
}

package() {
  cd "${pkgname}-${pkgver}"
  make VERSION="${pkgver}" PREFIX=/usr DESTDIR="${pkgdir}" install
  install -Dm644 COPYING "${pkgdir}/usr/share/licenses/${pkgname}/COPYING"
}
