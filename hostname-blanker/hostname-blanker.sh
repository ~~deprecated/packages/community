#!/bin/sh
# This script wipes hostname so that DHCP can't send it over the network
sed -i '/dhcp=/c\dhcp=dhclient' /etc/NetworkManager/NetworkManager.conf
echo "localhost" > /etc/hostname
wait
sed -i '/127.0.0.1/c\127.0.0.1       localhost.localdomain   localhost 'localhost'' /etc/hosts  ##properly sets our new host in /etc/hosts file
wait
sysctl kernel.hostname=localhost
wait
hostname localhost ##avoids need to reboot before showing our new hostname in terminals etc.
wait
