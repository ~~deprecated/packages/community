# Maintainer: Márcio Silva <coadde@hyperbola.info>
# Contributor: André Silva <emulatorman@hyperbola.info>
# based of glib2

pkgbase=glib2-static
pkgname=$pkgbase
pkgver=2.52.1
_debver=2.52.3
_debrel=1
pkgrel=1
pkgdesc="Low level core library (static libraries only)"
url="https://www.gtk.org/"
arch=(i686 x86_64 armv7h)
license=(LGPL)
makedepends=(python quilt)
source=("https://download.gnome.org/sources/${pkgname%2-static}/${pkgver:0:4}/${pkgname%2-static}-$pkgver.tar.xz"
        "https://deb.debian.org/debian/pool/main/g/glib2.0/glib2.0_$_debver-$_debrel.debian.tar.xz"
        skip-broken-timer-test.patch
        bad-dconf-paths.patch)
sha512sums=('6f0475b4ee1078867671a79b1751674926ba1caa872cec3896e2830c61c3cde67972afa9f2d606a2b6705e9846222462b003c6e966a9b63982f3fc3fb58c4d37'
            'cdd999e044719cf06a6b9e5d12dd72eba7785fd9e01d250aca18124179241208f4bf0a226bce4f79f6f385be73a8a92fcdd10b6b16e1d8bfbde50b01d54c65d0'
            'a53b03098abbdd83f7ca24be2c58183ee02c9a64814df840b5bc1678087d13e1b85d5dd5821a9df79649cbfc171484ccfeddbf60d771ffbba2d3c3de53cabca1'
            '8d5da70a3ef39771e8dc61e050b7425922012f2d4094bff64bc3efdef12974bd4fea853c52f099b47b6464bc65daf8de5796a2d93ffe2c1100e75d72ce5c9053')

prepare() {
  cd glib-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  else
    patch -Rp1 -i ../bad-dconf-paths.patch
  fi
}

build() {
  cd glib-$pkgver
  ./configure --prefix=/usr --libdir=/usr/lib \
      --disable-fam --enable-static
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
  make
}

check() {
  cd glib-$pkgver
#  make check
}

package() {
  depends=(glib2)
  options=(staticlibs)

  cd glib-$pkgver
  make completiondir=/usr/share/bash-completion/completions DESTDIR="$pkgdir" install

  # remove conflicting files
  rm -vr ${pkgdir}/usr/{bin,lib/{glib-*,pkgconfig},include,share}
  rm -v ${pkgdir}/usr/lib/lib*.so*
}
