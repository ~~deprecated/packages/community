# Maintainer (Arch): Andrew Crerar <andrew@crerar.io>
# Contributor (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor (Arch): Jan-Erik Rediger <badboy at archlinux dot us>
# Contributor (Arch): nofxx <x@<nick>.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=redis
pkgver=4.0.9
_debver=4.0.9
_debrel=2
pkgrel=1.hyperbola5.backports1
pkgdesc='Advanced key-value store, with logger recommendation and OpenRC support'
arch=('i686' 'x86_64')
url='https://redis.io/'
license=('Modified-BSD')
depends=('jemalloc' 'grep' 'shadow')
makedepends=('quilt')
optdepends=('logger: message logging support')
backup=('etc/redis.conf'
        'etc/logrotate.d/redis'
        'etc/conf.d/redis'
        'etc/conf.d/redis-sentinel')
install=redis.install
source=(http://download.redis.io/releases/redis-$pkgver.tar.gz
        https://deb.debian.org/debian/pool/main/r/redis/redis_$_debver-$_debrel.debian.tar.xz
        redis.logrotate
        redis.conf-sane-defaults.patch
        redis-2.8.11-use-system-jemalloc.patch
        redis.confd
        redis.initd
        redis-sentinel.confd
        redis-sentinel.initd)
sha512sums=('a6cf63cb361f0a87da3955ba628190dc04cad342f7a664d18e44416ee67dd86ed6e3a46b9701e994f1417e56b819b3c6fc595f363c10bb4b83d5033919d29598'
            '40c50e169ab007cfc0683550c7efdadff12630eb1a8ece7c0ef192cb16cc663db2ac1f43cc35d0b75158531b3744e9da4424485598e0157553fbcd43586c04d3'
            'df11492df0458b224f75fff31475d39b85116cba6deb06d80d0fd8c467d221db51a2a8f5fc5d2e3e8239c0718e1cf5dc12e99cac9019cb99d3f11835ad00aa5d'
            '9041a857a757ff16a31cfd538ef73b01ca13804c4499158e952169c9fad42045a36555edb25cb87a6b9e700fb5c147747b3b30c10f7ebea4db9d65946f396fae'
            '802d887b184227818f24c7a0cf4bd8a1d35e27504fbab05ac1c9abe71ae9e89d58e6c60a8e393ef46db563910ecaae58d6e2abab063e956c11d6d95aa2d61536'
            'f041e6b1da2c579a7dcf40e411ff47bc4675e5931e04f4353888ed5864b7e0c94d53fc98faee982c65a69d0154ee1f624fbadbbcc3a324aec583eefee6fb7133'
            'df47c84f96ea1737648941fae6ac74fafac7e7f167812ef5f7fb6e7f3087f7d35c0f6652b9671ad8095e0a6a5091b568d5f60f9309ea35e5a78ebb7164a07b01'
            'ea455f8c73c21bc46127ea5c7b99dc5707ad41160e5fd61367555dd2e3fd5c1518026257651e7d371a60a64b682b6839dd37580f75308c73239edeba49d6b372'
            'f1288a2c84b6d3312adaf199dd6165249ebe94927916a2d02f7b8d45ed1f205b03ac03805d9fcdb66a64b388bbf0d1f22b97415e2b239313e6ddf052f014f96f')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/debian-packaging/0007-Set-Debian-configuration-defaults.patch || true
    rm -v debian/patches/0004-SOURCE_DATE_EPOCH.patch || true

    quilt push -av
  else
    patch -p1 -i ../redis-2.8.11-use-system-jemalloc.patch
  fi

  patch -p1 -i ../redis.conf-sane-defaults.patch
}

build() {
  make -C $pkgname-$pkgver
}

package() {
  cd $pkgname-$pkgver
  make PREFIX="$pkgdir"/usr install

  install -Dm644 COPYING "$pkgdir"/usr/share/licenses/redis/COPYING
  install -Dm644 redis.conf "$pkgdir"/etc/redis.conf
  for f in redis redis-sentinel; do
    install -Dm644 "$srcdir"/$f.confd "$pkgdir"/etc/conf.d/$f
    install -Dm755 "$srcdir"/$f.initd "$pkgdir"/etc/init.d/$f
  done

  # files kept for compatibility with installations made before 2.8.13-2
  install -Dm644 ../redis.logrotate "$pkgdir"/etc/logrotate.d/redis

  ln -sf redis-server "$pkgdir"/usr/bin/redis-sentinel
}
