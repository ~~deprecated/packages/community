# Maintainer (Arch): Baptiste Jonglez <archlinux at bitsofnetworks dot org>
# Contributor (Arch): Andy Weidenbaum <archbaum@gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=libsecp256k1
_gitcommit=0b7024185045a49a1a6a4c5615bf31c94f63d9c4
pkgver=20170927+825+g0b70241
_debver=0.1~20170810
_debrel=2
pkgrel=1.hyperbola1
pkgdesc="Optimized C library for EC operations on curve secp256k1"
arch=('i686' 'x86_64')
url="https://github.com/bitcoin-core/secp256k1"
depends=("gmp")
makedepends=("quilt")
license=('MIT')
source=($pkgname-$pkgver.tar.gz::https://github.com/bitcoin-core/secp256k1/archive/${_gitcommit}.tar.gz
        https://deb.debian.org/debian/pool/main/libs/libsecp256k1/libsecp256k1_$_debver-$_debrel.debian.tar.xz)
sha512sums=('54e0c446ae63105800dfaf23dc934734f196c91f275db0455e58a36926c29ecc51a13d9b1eb2e45bc86199120c3c472ec7b39086787a49ce388a4df462a870bc'
            '14bb85f93f7b0ad33795bb856198c9b80c41243971c9beb4322037b2d01b2b90e20c48232225e10d98defeee8767ba8dc46c2cd693afa65d68bf7f751d318f29')

prepare() {
  cd "secp256k1-$_gitcommit"
  # Debian patches
  export QUILT_PATCHES=debian/patches
  export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
  export QUILT_DIFF_ARGS='--no-timestamps'

  mv "$srcdir"/debian .

  quilt push -av
}

build() {
  cd "secp256k1-$_gitcommit"
  ./autogen.sh
  ./configure \
    --prefix=/usr \
    --sbindir=/usr/bin \
    --libexecdir=/usr/bin \
    --sysconfdir=/etc \
    --sharedstatedir=/usr/share/libsecp256k1 \
    --localstatedir=/var/lib/libsecp256k1 \
    --disable-static \
    --disable-benchmark \
    --disable-coverage \
    --enable-module-recovery \
    --disable-jni \
    --enable-tests \
    --enable-exhaustive-tests \
    --disable-openssl-tests \
    --with-gnu-ld
  make
}

check() {
  cd "secp256k1-$_gitcommit"
  make check
}

package() {
  cd "secp256k1-$_gitcommit"
  install -Dm 644 COPYING -t "$pkgdir/usr/share/licenses/libsecp256k1"
  install -Dm 644 README.md -t "$pkgdir/usr/share/doc/libsecp256k1"
  make DESTDIR="$pkgdir" install
}
