# Contributor: Jesús E. <heckyel@hyperbola.info>

pkgname=emacs-which-key
pkgver=3.4.0
_debver=3.4.0
_debrel=1
pkgrel=1
pkgdesc='Emacs package that displays available keybindings in popup'
arch=('any')
url='https://github.com/justbur/emacs-which-key'
license=('GPL-3')
depends=('emacs')
makedepends=('quilt')
install='emacs-which-key.install'
source=("$pkgname-$pkgver.tar.gz::https://github.com/justbur/emacs-which-key/archive/v$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/e/emacs-which-key/emacs-which-key_$_debver-$_debrel.debian.tar.xz")
sha512sums=('1b9b448906dfce2f435ed98ec068b75ca82825a1ed75bce425b61fa79caeae56d093d76fd52143c6638afc64b43ce7f58b4a96788c507d83d7687fbcc6543a61'
            'bc73a9bf6000562168e241440e61b55a891e29de976ce4f1ffe236ef5c44cb1fa54dca5e5a335012743acfe8e02821416b790077ee4f85e6bf06dc818fe85043')

prepare() {
  cd "$pkgname-$pkgver"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "$pkgname-$pkgver"

  emacs -q --no-splash -batch -L . -f batch-byte-compile which-key.el
}

package() {
  cd "$pkgname-$pkgver"

  install -d "$pkgdir/usr/share/emacs/site-lisp/which-key"
  for i in which-key.el{,c}; do
    install -m644 "$i" "$pkgdir/usr/share/emacs/site-lisp/which-key/$i"
  done

  # doc
  install -d "$pkgdir/usr/share/doc/$pkgname/"
  install -m644 README.org "$pkgdir/usr/share/doc/$pkgname/README.org"

  # license
  install -d "$pkgdir/usr/share/licenses/$pkgname"
  install -m644 LICENSE.md "$pkgdir/usr/share/licenses/$pkgname/LICENSE.md"

}

# vim:set ts=2 sw=2 et:
