# Firejail profile for iceape-uxp
# This file is overwritten after every install/update
# Persistent local customizations
include /etc/firejail/iceape-uxp.local
# Persistent global definitions
include /etc/firejail/globals.local

noblacklist ${HOME}/.cache/hyperbola
noblacklist ${HOME}/.hyperbola
noblacklist ${HOME}/.pki

include /etc/firejail/disable-common.inc
include /etc/firejail/disable-devel.inc
include /etc/firejail/disable-interpreters.inc
include /etc/firejail/disable-programs.inc

mkdir ${HOME}/.cache/hyperbola
mkdir ${HOME}/.hyperbola
whitelist ${DOWNLOADS}
whitelist ${HOME}/.cache/gnome-mplayer/plugin
whitelist ${HOME}/.cache/hyperbola
whitelist ${HOME}/.config/gnome-mplayer
whitelist ${HOME}/.keysnail.js
whitelist ${HOME}/.lastpass
whitelist ${HOME}/.hyperbola
whitelist ${HOME}/.pentadactyl
whitelist ${HOME}/.pentadactylrc
whitelist ${HOME}/.pki
whitelist ${HOME}/.vimperator
whitelist ${HOME}/.vimperatorrc
whitelist ${HOME}/.zotero
whitelist ${HOME}/dwhelper
include /etc/firejail/whitelist-common.inc

caps.drop all
netfilter
nodvd
nonewprivs
noroot
notv
protocol unix,inet,inet6,netlink
seccomp

disable-mnt
# private-etc passwd,group,hostname,hosts,localtime,nsswitch.conf,resolv.conf,gtk-2.0,pango,fonts,iceweasel-uxp,icedove-uxp,mime.types,mailcap,asound.conf,pulse,machine-id
