# Firejail profile for icedove-uxp
# This file is overwritten after every install/update
# Persistent local customizations
include /etc/firejail/icedove-uxp.local
# Persistent global definitions
include /etc/firejail/globals.local

# Users have icedove-uxp set to open a browser by clicking a link in an email
# We are not allowed to blacklist browser-specific directories

noblacklist ${HOME}/.cache/hyperbola/icedove-uxp
noblacklist ${HOME}/.gnupg
noblacklist ${HOME}/.hyperbola/icedove-uxp

mkdir ${HOME}/.cache/hyperbola/icedove-uxp
mkdir ${HOME}/.gnupg
mkdir ${HOME}/.hyperbola/icedove-uxp
whitelist ${HOME}/.cache/hyperbola/icedove-uxp
whitelist ${HOME}/.gnupg
whitelist ${HOME}/.hyperbola/icedove-uxp
include /etc/firejail/whitelist-common.inc

ignore private-tmp

# allow browsers
# Redirect
include /etc/firejail/firefox.profile
