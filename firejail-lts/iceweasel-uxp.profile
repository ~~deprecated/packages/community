# Firejail profile for iceweasel-uxp
# This file is overwritten after every install/update
# Persistent local customizations
include /etc/firejail/iceweasel-uxp.local
# Persistent global definitions
include /etc/firejail/globals.local

noblacklist ${HOME}/.cache/hyperbola
noblacklist ${HOME}/.hyperbola

mkdir ${HOME}/.cache/hyperbola/iceweasel-uxp
mkdir ${HOME}/.hyperbola
whitelist ${HOME}/.cache/hyperbola/iceweasel-uxp
whitelist ${HOME}/.hyperbola

# private-etc must first be enabled in firefox-common.profile
#private-etc iceweasel

# Redirect
include /etc/firejail/firefox.profile
