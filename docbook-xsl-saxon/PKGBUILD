# Maintainer (Parabola): Omar Vega Ramos <ovruni@gnu.org.pe>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=docbook-xsl-saxon
pkgver=1.00
_debver=1.00
_debrel=6
pkgrel=3.hyperbola1
pkgdesc="Java extensions for use with the DocBook XML stylesheets and the Saxon XSLT engine"
arch=('any')
url="http://docbook.sourceforge.net/"
license=('custom:MIT')
depends=('java-runtime' 'docbook-xsl' 'saxon6')
makedepends=('apache-ant' 'saxon6' 'jh' 'quilt')
source=("https://sourceforge.net/projects/docbook/files/${pkgname}/${pkgver}/${pkgname}-${pkgver}.tar.bz2"
        "https://deb.debian.org/debian/pool/main/d/docbook-xsl-saxon/docbook-xsl-saxon_$_debver.dfsg.1-$_debrel.debian.tar.xz"
        'pom.xml')
sha512sums=('e3920b23cd760ee7c24b7ee1af9d360c9d8ec4cc233cc122789fc5cf82b99a058da4f5786e5f9863d6dcf0403d77a6b91e0f07f33630d7e0fc31aeb16b4d2fda'
            '934f6c1000e1e5841550ed3b8d606bc2d5adf243cccc7c1dffc75ad29963644e14d69f5854bf3a6c575bf7d7de850835a514989692f446803635cca749c6a332'
            '366b095fe88930a26c6cd8771d90b4bdd8b649112f6091f261b25e27abd80be6cbc962ab335b9122d7f87f2a354e550770cd9a195f97ee043aec87719fe3edd5')

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  if [[ $pkgver = $_debver ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  rm -rv *.jar
  rm -rv {classes,javadoc}
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  ant \
    -Dplatform.active=hyperbola \
    -Dplatforms.hyperbola.home=/usr/lib/jvm/default \
    -Djavac.classpath=/usr/share/java/saxon.jar \
    -Ddist.jar=${pkgname}.jar
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  # Install license file
  install -Dm644 COPYING "${pkgdir}/usr/share/licenses/${pkgname}/COPYING"

  # Install documentation
  install -d "${pkgdir}/usr/share/doc/${pkgname}"
  cp -r javadoc "${pkgdir}/usr/share/doc/${pkgname}"

  # Install Maven artifacts
  export DESTDIR=${pkgdir}
  jh mvn-install net.sf.docbook ${pkgname} ${pkgver} \
    "${srcdir}/pom.xml" \
    "${pkgname}.jar" \
    "${pkgname}-${pkgver}.jar"
  ln -s "/usr/share/java/${pkgname}-${pkgver}.jar" \
    "${pkgdir}/usr/share/java/${pkgname}.jar"
}
