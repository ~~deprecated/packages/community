# Maintainer (Arch): Lukas Fleischer <lfleischer@archlinux.org>
# Contributor (Arch): Kaiting Chen <kaitocracy@gmail.com>
# Contributor (Arch): Aaron Schaefer <aaron@elasticdog.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=duplicity
pkgver=0.7.12
_debver=0.7.12
_debrel=1
pkgrel=1.hyperbola1
pkgdesc='A utility for encrypted, bandwidth-efficient backups using the rsync algorithm, without python2-boto, python2-dropbox and python2-gdata recommendation'
arch=('i686' 'x86_64' 'armv7h')
url='http://www.nongnu.org/duplicity/'
license=('GPL')
depends=('gnupg' 'librsync' 'ncftp' 'python2-paramiko' 'python2-lockfile')
makedepends=('python2-setuptools' 'quilt')
optdepends=('lftp: FTPS backend'
            'python2-gobject: GIO backend'
            'gvfs: GIO backend'
            'rsync: rsync backend')
source=("https://launchpad.net/$pkgname/0.7-series/${pkgver}/+download/$pkgname-$pkgver.tar.gz"{,.sig}
        "https://deb.debian.org/debian/pool/main/d/duplicity/duplicity_$_debver-$_debrel.debian.tar.xz")
sha512sums=('8a532e7bb548c170184666a0ba2b41a15f3002c14edd64b1b1eb4df862647fc6dfde797d6ecf38e326d7c160c31ff521baf351e520835495e1d0e2ddbdb19f0f'
            'SKIP'
            '8ae9ad91d5b8ec25dfbf07f7e95fa0578b06d3fc27a8e92d4872d5614b08f67d3118823097b908d7fca4ac30c125b5963338429b9e9b8b651fceabf5f13271a0')
validpgpkeys=('9D95920CED4A8D5F8B086A9F8B6F8FF4E654E600')

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  if [[ $pkgver = $_debver ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian/patches debian

    quilt push -av
  fi
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  # python2 fix
  for file in $(find . -name '*.py' -print); do
    sed -i 's_^#!.*/usr/bin/python$_#!/usr/bin/python2_' $file
    sed -i 's_^#!.*/usr/bin/env.*python$_#!/usr/bin/env python2_' $file
  done
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  python2 setup.py install --root="$pkgdir" --optimize=1

  # fix broken documentation permissions until upstream does (FS#27301)
  chmod 644 "$pkgdir/usr/share/man/man1/$pkgname.1"
  chmod 644 "$pkgdir/usr/share/man/man1/rdiffdir.1"
  chmod 644 "$pkgdir/usr/share/doc/$pkgname-$pkgver"/*
}
