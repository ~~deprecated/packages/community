# Maintainer (Arch): Anatol Pomozov <anatol pomozov at gmail>
# Contributor (Arch): Daniel YC Lin <dlin.tw at gmail>
# Contributor (Arch): Konstantin Plotnikov <kostyapl at gmail dot com>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=fossil
pkgver=2.2
_debver=$pkgver
_debrel=1
pkgrel=1.hyperbola2
pkgdesc='Simple, high-reliability, distributed software configuration management, with libressl and OpenRC support'
arch=(i686 x86_64)
license=(Simplified-BSD)
url='https://www.fossil-scm.org'
depends=(libressl zlib sqlite)
makedepends=(quilt)
backup=(etc/conf.d/fossil)
source=(
  https://deb.debian.org/debian/pool/main/f/fossil/fossil_$_debver.orig.tar.gz
  https://deb.debian.org/debian/pool/main/f/fossil/fossil_$_debver-$_debrel.debian.tar.xz
  fossil.confd
  fossil.initd
)
sha512sums=('1a1aedf3f9883a64f9f98c9a01857f5fb69cff0be23817016a4bc405b2efc85f21ac86a771d5e7314128e3fabc21444ac717017cf0935241fd4af964b7c086e0'
            '9666277345bb124dc89691f122482902d90b556f8b64fc3078bdccac0fa249f2b689396ea70e6c4b76e89931757d97282558905d00d13e5995da21a2e1aef1ba'
            'b1a0de73301dde41083b762b6dd8246ecdbb52c62d8cd34825c1357bb730fb5e125b45ad621a2bea783822e910bc45bd373ca0590d0df65eb913d9b505cdc598'
            'd6d3cb9928c997d48507417881fc3f0f65a6ff83a56f8e1eb2db3e3f949cd900c236b9af9a82055f5c67053b3772964a656e152a6f2a3471d1dd47be1441147d')

prepare() {
  cd fossil-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian/patches debian

    # Doesn't apply and seems unimportant
    rm -v debian/patches/debian-changes || true

    quilt push -av
  fi
}

build() {
  cd fossil-$pkgver
  ./configure --prefix=/usr --json --disable-internal-sqlite
  # headers and translate targets are problematic with parallel jobs
  #make -j1 bld bld/headers
  make
}

package() {
  cd fossil-$pkgver
  install -Dm755 fossil "$pkgdir"/usr/bin/fossil
  install -Dm644 tools/fossil-autocomplete.bash "$pkgdir"/usr/share/bash-completion/completions/fossil
  install -Dm644 COPYRIGHT-BSD2.txt "$pkgdir"/usr/share/licenses/$pkgname/COPYRIGHT-BSD2.txt
  install -Dm644 "$srcdir"/fossil.confd "$pkgdir"/etc/conf.d/fossil
  install -Dm755 "$srcdir"/fossil.initd "$pkgdir"/etc/init.d/fossil
}
