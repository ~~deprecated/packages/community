# $Id$
# Maintainer (Arch): Jonathan Steel <jsteel at archlinux.org>
# Contributor (Arch): Felix Yan <felixonmars@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgbase=python-boto3
pkgname=('python-boto3' 'python2-boto3')
pkgver=1.4.8
_debver=1.4.2
_debrel=1
pkgrel=1.hyperbola1
pkgdesc='The AWS SDK for Python'
arch=('any')
license=('Apache')
url='https://github.com/boto/boto3'
makedepends=('python-setuptools' 'python2-setuptools')
# checkdepends=('python-coverage' 'python-nose' 'python-mock')
source=("$pkgname-$pkgver.tar.gz::https://github.com/boto/boto3/archive/$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/p/python-boto3/python-boto3_$_debver-$_debrel.debian.tar.xz")
sha512sums=('91985508b02eb24e5770c51b17faca2e0cf194a1ca4cfc8313d296fee25001be729d9a5d5f9960bc3d756c6eefbb8694cbb931cb0109704401c4255f0486cb53'
            '17196d9d9197db6a20d20c0fc26df73372706a7161900f6d64afda7278955ba1ecd8c46e10d65ee5c34149e51c6728dd9c64874e7c625e3721c030d70f5d625e')

prepare() {
  cd "$srcdir"/boto3-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/do-not-use-vendored-code.patch || true

    quilt push -av
  fi

  cd ..

  cp -r "boto3-$pkgver" "python-boto3-$pkgver"
  cp -r "boto3-$pkgver" "python2-boto3-$pkgver"
}

build() {
  cd "$srcdir/python-boto3-$pkgver"
  python setup.py build

  cd "$srcdir/python2-boto3-$pkgver"
  python2 setup.py build
}

check() {
  cd "$srcdir"/boto3-$pkgver
  # scripts/ci/run-tests
}

package_python-boto3() {
  depends=('python-botocore' 'python-jmespath' 'python-s3transfer')
  cd "$srcdir/python-boto3-$pkgver"
  python setup.py install --root="$pkgdir" --optimize=1
}

package_python2-boto3() {
  depends=('python2-botocore' 'python2-jmespath' 'python2-s3transfer')
  cd "$srcdir/python2-boto3-$pkgver"
  python2 setup.py install --root="$pkgdir" --optimize=1
}
