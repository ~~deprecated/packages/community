# Maintainer (Arch): Maxime Gauduin <alucryd@archlinux.org>
# Contributor (Arch): Stéphane Gaudreault <stephane@archlinux.org>
# Contributor (Arch): BlackEagle <ike.devolder@gmail.com>
# Contributor (Arch): Dany Martineau <dany.luc.martineau@gmail.com>
# Contributor (Parabola): Kete <kete@ninthfloor.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

pkgname=clementine
_pkgname=clementine-libre
pkgver=1.3.1
_debver=1.3.1
_debrel=1
pkgrel=7.hyperbola3
pkgdesc='A modern music player and library organizer, without nonfree artwork and Spotify support and without support for services that are unsafe and dangerous for privacy'
url='https://www.clementine-player.org/'
license=('GPL-3')
arch=('i686' 'x86_64')
depends=('chromaprint' 'crypto++' 'fftw' 'gcc-libs' 'glew' 'glib2' 'glibc'
         'gst-plugins-base-libs' 'gstreamer' 'libcdio' 'libechonest' 'libgl'
         'libmtp' 'libpulse' 'libx11' 'protobuf' 'qjson'
         'qt4' 'sqlite' 'taglib' 'zlib')
makedepends=('boost' 'cmake' 'mesa' 'sparsehash' 'quilt')
optdepends=('gst-plugins-base: "Base" plugin libraries'
            'gst-plugins-good: "Good" plugin libraries'
            'gst-plugins-bad: "Bad" plugin libraries'
            'gst-plugins-ugly: "Ugly" plugin libraries'
            'gst-libav: Libav plugin'
            'gvfs: Various devices support')
mksource=("${pkgname}-${pkgver}.tar.gz::https://github.com/$pkgname-player/${pkgname^}/archive/${pkgver}.tar.gz")
source=("https://repo.hyperbola.info:50000/sources/${_pkgname}/${_pkgname}-${pkgver}.tar.gz"{,.sig}
        "https://deb.debian.org/debian/pool/main/c/clementine/clementine_$_debver+git276-g3485bbe43+dfsg-$_debrel.debian.tar.xz"
        clementine-gcc6.patch::"https://github.com/clementine-player/Clementine/commit/e31278.patch"
        clementine-1.3.1-chromaprint-1.4.0.patch::"https://github.com/clementine-player/Clementine/commit/ded3126.patch"
        remove-nonfree-artwork-and-spotify.patch
        moognu.png
        privacy.patch)
mksha512sums=('1942a046be24a583b5e2095dd39dc736c1b14a47dc4aad61d6252c1fccf745c26d7b35da907a220d5dc8c0736f5b589d050ed9582de343aa3cb5abc607001790')
sha512sums=('842709b1b8c86640eceb7c28791aa47e40f0a2b51d7003b54eb68215d4554b7471043f864a91802a3370e3497ee00e27ae4169e526d08015e76f30c8429b3233'
            'SKIP'
            'ed9e217db5eedb1ccfae476c5e7eb38e58500d8be149bd9747c0c981f06f7662501e607b01f59798954876254e9009cab1f297de2a181ecb926f4b3ef57db09e'
            'b8fb09d1475fe5eb4b288db18c82432b81507b59f452976e3e5862cd207047d224165161b5aaac72bd12f9153991185519a4f669d7fcb0f13240c0c04d64b9c7'
            '05d2c6fc290f4fbfc380b3887b20ed0344aeb98a24ba435e0934477be374a68f893d3971aac65ce9b61e87df97a196dfd7643f9161a61f8de03e32a9bb8c9435'
            '08803dc96cead1fe3f04a0ada47b26435a0f576c8669d7e7729ccbf53f043a7609315ec01de8d811e3e67ace4c1195371dc52b675b2963588028ed5970fc5528'
            '321deda4e6a7b1f93cf6b4a87f668acd9460e49959f168ea41a41a8af1040308d366b625d98f4dc30f1be133b36b951c15d061ce45afec0777fff70e56cf1591'
            'b80b61aa08eb2716c5cd052d3846dd5a1c94c5e0895820e881b78b6884c2127ca5d3afb6ef8c357c76e74caafa13c628748eb310a2fb0ff84f4bf17734359640')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

mksource() {
  cd ${pkgname^}-${pkgver}

  # remove nonfree artwork for the Nyan Cat plugin
  # https://labs.parabola.nu/issues/329
  rm -v data/nyancat.png
}

prepare() {
  cd ${pkgname^}-${pkgver}

  if [[ $pkgver = $_debver ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    rm -r debian
    mv "$srcdir"/debian .

    quilt push -av
  fi

  # remove nonfree artwork and Spotify references
  patch -Np1 -i ../remove-nonfree-artwork-and-spotify.patch

  # remove Spotifyblob files
  rm -rv ext/{$pkgname-spotifyblob,lib${pkgname}-spotifyblob}

  # remove Spotify files
  rm -rv src/internet/spotify
  rm -v data/icons/svg/spotify.svg
  rm -v data/providers/{22x22,32x32,48x48}/spotify.png
  rm -v data/spotify-attribution.png
  rm -v data/clementine-spotify-public.pem
  rm -v cmake/SpotifyVersion.cmake
  rm -v src/globalsearch/spotifysearchprovider.{cpp,h}

  # move free Nyan Cat replacement mascot (MooGNU) to the source code
  # https://labs.parabola.nu/issues/329
  install -m644 -v ../moognu.png data

  # remove support for services that are unsafe and dangerous for privacy
  # https://issues.hyperbola.info/index.php?do=details&task_id=1437
  patch -p1 -i ../privacy.patch

  # fix build with GCC 6
  patch -p1 -i ../clementine-gcc6.patch
  # fix build with chromaprint 1.4.0
  patch -p1 -i ../clementine-1.3.1-chromaprint-1.4.0.patch

  if [[ -d build ]]; then
    rm -rf build
  fi
  mkdir build
}

build() {
  cd ${pkgname^}-${pkgver}/build

  cmake .. \
    -DCMAKE_BUILD_TYPE='Release' \
    -DCMAKE_INSTALL_PREFIX='/usr'

  make
}

package() {
  cd ${pkgname^}-${pkgver}/build

  make DESTDIR="${pkgdir}" install
  install -Dm644 ../COPYING ${pkgdir}/usr/share/licenses/${pkgname}/COPYING
}

# vim: ts=2 sw=2 et:
