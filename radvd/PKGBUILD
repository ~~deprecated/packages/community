# Maintainer (Arch): Sébastien Luttringer
# Maintainer (Arch): Thore Bödecker <foxxx0@archlinux.org>
# Contributor (Arch): Kaiting Chen <kaitocracy@gmail.com>
# Contributor (Arch): Mark Smith <markzzzsmith@yahoo.com.au>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=radvd
pkgver=2.17
_debver=2.17
_debrel=2~bpo9+1
pkgrel=2.hyperbola3
pkgdesc='IPv6 Router Advertisement Daemon, with logger recommendation and OpenRC support'
url='http://www.litech.org/radvd/'
license=('custom:radvd')
depends=('glibc')
makedepends=('check' 'quilt')
optdepends=('logger: message logging support')
arch=('i686' 'x86_64')
backup=('etc/radvd.conf')
validpgpkeys=('B11F2EED32FB6728F700337C411FA8C112D91A31') # Reuben Hawkins
source=("http://www.litech.org/radvd/dist/${pkgname}-${pkgver}.tar.xz"{,.asc}
        "https://deb.debian.org/debian/pool/main/r/radvd/radvd_$_debver-$_debrel.debian.tar.xz"
        'avoid-premature-warning-about-all-zeros-prefix.patch'
        'radvd.initd')
sha512sums=('f62aa8acb423320423637c7908abe1d3b8ec4b32f78a81ba67c1a4ecf9bcaaae4e3671fc0c2c5b5776edfe5e6b5d3bf1daf60e55387dd0c0d7370a86d867873c'
            'SKIP'
            '70b04545f8880040c912f8f19aec8ab956373c0154eefff55c24406a6cbc1aa706219f9c9d68019f0c5c743a679387db92560cc414f48301027c3f65d30f10b2'
            '413ce0c04f1024d9bfba34911834e9db4a2d30e6c270f989f63c33cd04deabb87627ea2e62a364ac80e44c5501c8ed5afd50f8d7446725240815cae95ae086d5'
            'd9a93882e677be2aee8e77f9a6c8498a6cf49f78621e29e10f710f0846e8247ce29942dd8ef0e07fda262eb3828b314e1eff706b4fe636ceface48dafed02daf')

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/kfreebsd.patch || true

    quilt push -av
  fi

  patch -p1 -i ../avoid-premature-warning-about-all-zeros-prefix.patch
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --with-pidfile=/run/radvd.pid \
    --with-check \
    --mandir=/usr/share/man
  make
}

check() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  make check
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}" install
  # default config
  install -D -m 644 radvd.conf.example "${pkgdir}/etc/radvd.conf"
  # license
  install -D -m 644 COPYRIGHT "${pkgdir}/usr/share/licenses/${pkgname}/COPYRIGHT"
  # openrc
  install -D -m 755 "${srcdir}/radvd.initd" "${pkgdir}/etc/init.d/radvd"
}

# vim:set ts=2 sw=2 et:
