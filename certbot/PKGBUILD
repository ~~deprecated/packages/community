# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Contributor (Arch): Gordian Edenhofer <gordian.edenhofer[at]yahoo[dot]de>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=certbot
pkgver=0.28.0
_debver=0.28.0
_debrel=1~bpo9+1
pkgrel=1.hyperbola1.backports2
pkgdesc="A tool to automatically receive and install X.509 certificates to enable TLS on servers. The client will interoperate with the Let’s Encrypt CA which will be issuing browser-trusted certificates for free."
arch=('any')
license=('Apache')
url="https://certbot.eff.org"
makedepends=('quilt')
depends=('ca-certificates' 'python-acme' 'python-configargparse' 'python-configobj'
         'python-cryptography' 'python-mock' 'python-parsedatetime' 'python-pyrfc3339'
         'python-pytz' 'python-setuptools' 'python-zope-component' 'python-zope-interface')
checkdepends=('python-pytest-runner')
optdepends=('certbot-apache: Apache plugin for Let’s Encrypt client'
            'certbot-nginx: Nginx plugin for Let’s Encrypt client')
replaces=("letsencrypt")
source=("https://pypi.io/packages/source/c/$pkgname/$pkgname-$pkgver.tar.gz"{,.asc}
        "https://deb.debian.org/debian/pool/main/p/python-certbot/python-certbot_$_debver-$_debrel.debian.tar.xz")
validpgpkeys=('148C30F6F7E429337A72D992B00B9CC82D7ADF2C'
              '1B41B758F9A78C7F444C20FA3E5B4152E3ADB1FD'
              'A2CFB51FA275A7286234E7B24D17C995CD9775F2')
sha512sums=('c6390190e2017d16eb518f2ae2920bb43a6b88ae70dfba6a18c52156652aac5bd0eedc62b04b270b91fb4753e5c30f072f211b184171b21f6a3fbe77f90e9d06'
            'SKIP'
            '406fed68065cf1797538195063c5a9ea2671bf23e8ed669e80d033c3a10d2824e8516040a3d549df89b7f99293e44fabed756fd0b7b35bbe7c8f48e1b7fa9a41')
prepare() {
  cd "$srcdir"/$pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "$srcdir"/$pkgname-$pkgver
  python setup.py build
}

check() {
  cd "$srcdir"/$pkgname-$pkgver
  python setup.py pytest
}

package() {
  cd "$srcdir"/$pkgname-$pkgver
  python setup.py install --root="$pkgdir"

  mkdir -p "$pkgdir"/{etc,var/lib,var/log}/letsencrypt
  chmod 700 "$pkgdir"/var/log/letsencrypt
}
