# Maintainer: André Silva <emulatorman@hyperbola.info>

## "1" to enable IA32 SHELL build in x86_64, "0" to disable
_SHELL_IA32_X64=1

###############
[[ $CARCH == x86_64 ]] && _TIANO_ARCH=X64
[[ $CARCH == i686 ]] && _TIANO_ARCH=IA32

[[ $CARCH == x86_64 ]] && _TIANO_S_ARCH=x64
[[ $CARCH == i686 ]] && _TIANO_S_ARCH=ia32
###############

pkgname=edk2-uefi-shell
pkgver=2017
pkgrel=1.backports1
pkgdesc="UEFI Shell v2 from Tianocore EDK2"
url='https://github.com/tianocore/edk2'
arch=(x86_64 i686)
license=(Simplified-BSD)
replaces=(edk2-uefi-shell-v2)
conflicts=(edk2-uefi-shell-v2)
makedepends=(python2 nasm)
options=(!strip !makeflags)
install=$pkgname.install
source=(https://github.com/tianocore/edk2/archive/vUDK$pkgver.tar.gz)
sha512sums=('d941a661d570b0f69962105920b3524c4aac1f4883468914305eb68015148d70f6593c52872a79a6143cf87dc44e9ce2b8957834b4524cb8a04b12473c6d7d81')

prepare() {
  cd $srcdir/edk2-vUDK$pkgver

  msg "Cleanup UDK config files"
  rm -rf Build
  rm -rf Conf
  mkdir -p Conf
  mkdir -p Build

  msg "Delete bogus dependency files"
  find . -name '*.d' -delete

  msg "Disable build ID generation"
  sed -i 's|,--gc-sections|,--gc-sections,--build-id=none|g' BaseTools/Conf/tools_def.template

  msg "Use python2 for UDK BaseTools"
  sed -i 's|python |python2 |g' BaseTools/BinWrappers/PosixLike/*
  sed -i 's|python |python2 |g' BaseTools/Tests/GNUmakefile

  msg "Fix GCC Warning as error"
  sed -i 's|-Werror |-Wno-error -Wno-unused-but-set-variable |g' BaseTools/Source/C/Makefiles/header.makefile
  sed -i 's|-Werror |-Wno-error -Wno-unused-but-set-variable |g' BaseTools/Conf/tools_def.template

  msg "Remove GCC -g debug option and add -O0 -mabi=ms -maccumulate-outgoing-args"
  sed -i 's|DEFINE GCC_ALL_CC_FLAGS            = -g |DEFINE GCC_ALL_CC_FLAGS            = -O0 -mabi=ms -maccumulate-outgoing-args |g' BaseTools/Conf/tools_def.template
  sed -i 's|DEFINE GCC44_ALL_CC_FLAGS            = -g |DEFINE GCC44_ALL_CC_FLAGS            = -O0 -mabi=ms -maccumulate-outgoing-args |g' BaseTools/Conf/tools_def.template

  msg "Fix UDK Target Platform"
  sed -i 's|ACTIVE_PLATFORM       = Nt32Pkg/Nt32Pkg.dsc|ACTIVE_PLATFORM       = ShellPkg/ShellPkg.dsc|g' BaseTools/Conf/target.template
  sed -i 's|TARGET                = DEBUG|TARGET                = RELEASE|g' BaseTools/Conf/target.template
  sed -i 's|TOOL_CHAIN_TAG        = MYTOOLS|TOOL_CHAIN_TAG        = GCC5|g' BaseTools/Conf/target.template
  sed -i 's|IA32|X64|g' BaseTools/Conf/target.template

  chmod 0755 BaseTools/BuildEnv
}

build() {
  cd $srcdir/edk2-vUDK$pkgver

  msg "Unset all compiler FLAGS"
  unset CFLAGS
  unset CPPFLAGS
  unset CXXFLAGS
  unset LDFLAGS
  unset MAKEFLAGS

  msg "Setup UDK Environment"
  source $srcdir/edk2-vUDK$pkgver/BaseTools/BuildEnv BaseTools
  echo

  msg "Compile UDK BaseTools"
  make -C BaseTools
  echo

  msg "Unset all compiler FLAGS"
  unset CFLAGS
  unset CPPFLAGS
  unset CXXFLAGS
  unset LDFLAGS
  unset MAKEFLAGS

  msg "Compile UEFI Shell v2 ${_TIANO_ARCH} binary"
  BaseTools/BinWrappers/PosixLike/build -p ShellPkg/ShellPkg.dsc -a ${_TIANO_ARCH} -b RELEASE -t GCC5
  echo

  if [[ $CARCH == x86_64 ]] && [[ $_SHELL_IA32_X64 == 1 ]]; then
    msg "Unset all compiler FLAGS"
    unset CFLAGS
    unset CPPFLAGS
    unset CXXFLAGS
    unset LDFLAGS
    unset MAKEFLAGS

    msg "Compile UEFI Shell v2 IA32 binary"
    BaseTools/BinWrappers/PosixLike/build -p ShellPkg/ShellPkg.dsc -a IA32 -b RELEASE -t GCC5
    echo
  fi
}

package() {
  msg "Install the UEFI Shell v2 ${_TIANO_ARCH} binary"
  install -d $pkgdir/usr/share/uefi-shell
  install -D -m0644 $srcdir/edk2-vUDK$pkgver/Build/Shell/RELEASE_GCC5/${_TIANO_ARCH}/Shell.efi $pkgdir/usr/share/uefi-shell/shell${_TIANO_S_ARCH}_v2.efi

  if [[ $CARCH == x86_64 ]] && [[ $_SHELL_IA32_X64 == 1 ]]; then
    msg "Install the UEFI Shell v2 IA32 binary"
    install -d $pkgdir/usr/share/uefi-shell
    install -Dm0644 $srcdir/edk2-vUDK$pkgver/Build/Shell/RELEASE_GCC5/IA32/Shell.efi $pkgdir/usr/share/uefi-shell/shellia32_v2.efi
  fi

  msg "Install license"
  install -Dm644 $srcdir/edk2-vUDK$pkgver/ShellPkg/License.txt $pkgdir/usr/share/licenses/$pkgname/License.txt
}
