# Maintainer (Arch): Levente Polyak <anthraxx[at]archlinux[dot]org>
# Contributor (Arch): Kuba Serafinowski <zizzfizzix(at)gmail(dot)com>
# Contributor (Arch): Daichi Shinozaki <dsdseg@gmail.com>
# Contributor (Arch): skydrome <skydrome@i2pmail.org>
# Contributor (Arch): MTsoul
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=websocketpp
pkgver=0.7.0
_debver=0.7.0
_debrel=7
pkgrel=1.hyperbola1
pkgdesc='C++/Boost Asio based websocket client/server library, with libressl recommendation'
url='https://www.zaphoyd.com/websocketpp/'
arch=('any')
license=('Modified-BSD')
optdepends=(
  'libressl: TLS socket component support'
  'asio: alternative asio transport support'
  'boost: non C++11 environments support'
  'boost-libs: non C++11 environments support'
)
makedepends=('cmake' 'boost' 'boost-libs' 'scons' 'quilt')
source=(${pkgname}-${pkgver}.tar.gz::https://github.com/zaphoyd/${pkgname}/archive/${pkgver}.tar.gz
        https://deb.debian.org/debian/pool/main/w/websocketpp/websocketpp_$_debver-$_debrel.debian.tar.xz
        minor-adjustments-to-recent-extension-negotiation.patch
        zlib-permessage-deflate.patch)
sha512sums=('91a86d4f5120db3f474169bb146f865f82167b1e9eedabec8793b31005e4ce3d22083283bc1b9f9e37fa0da835addcb2b68260a27c753852c06b3b1bb2f3c12e'
            '4f415dcb54df7082cd147ff4202f6f58134151ecc0f5eaec3b36ed3fe26ce9766f531f2dd20ac3e8ead27e37aa5336878d4b6a14c65d8941289f1c8c142b7f64'
            'd7331cf1b543e13b8cc411897886729c4f8b6e0d5720ca25337b7d3c92392be94586f418167ab9426e0f9eb15f2f0fcac8b7618a0657b05c1130ef22d82043d8'
            'd1cddd56c8ed04c1fddb4eae550b232bbadd8926460015001a2c0a6770078167cb68d6e208cdb5d2b9089299bec5d404dae2a5022cbd75c3796dfb6972d34e8e')

prepare() {
  cd ${pkgname}-${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  patch -p1 -i ../zlib-permessage-deflate.patch
  patch -p1 -i ../minor-adjustments-to-recent-extension-negotiation.patch
}

build() {
  cd ${pkgname}-${pkgver}
  WSPP_ENABLE_CPP11=1 \
    BOOST_LIBS=/usr/lib \
    BOOST_INCLUDES=/usr/include/boost \
    scons "${MAKEFLAGS}"
  (cd build
    cmake -DCMAKE_INSTALL_PREFIX=/usr ..
  )
}

check() {
  cd ${pkgname}-${pkgver}
  WSPP_ENABLE_CPP11=1 \
    BOOST_LIBS=/usr/lib \
    BOOST_INCLUDES=/usr/include/boost \
    scons test
}

package() {
  cd ${pkgname}-${pkgver}
  make -C build DESTDIR="${pkgdir}" install
  install -Dm 644 COPYING "${pkgdir}/usr/share/licenses/${pkgname}/COPYING"
  install -Dm 644 readme.md "${pkgdir}/usr/share/doc/${pkgname}/readme.md"
}

# vim: ts=2 sw=2 et:
