# Maintainer (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): Jonathan Wiersma <archaur at jonw dot org>
# Contributor (Artix): artoo <artoo@cromnix.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Jesús E. <heckyel@hyperbola.info>

pkgname=libvirt
pkgver=3.2.0
_debver=3.0.0
_debrel=4+deb9u4
pkgrel=2.hyperbola10
pkgdesc="API for controlling virtualization engines (openvz,kvm,qemu,xen,etc), with eudev, libressl and OpenRC support"
arch=('i686' 'x86_64')
url="https://libvirt.org/"
license=('LGPL-2.1')
depends=('e2fsprogs' 'gnutls' 'iptables' 'libxml2' 'parted' 'polkit' 'python2'
	 'avahi' 'yajl' 'libpciaccess' 'eudev' 'dbus' 'libxau' 'libxdmcp' 'libpcap' 'libcap-ng'
	 'curl' 'libsasl' 'libgcrypt' 'libgpg-error' 'libressl' 'libxcb' 'gcc-libs'
	 'iproute2' 'libnl' 'libx11' 'numactl' 'gettext' 'ceph' 'libssh2' 'netcf')
makedepends=('pkgconfig' 'lvm2' 'linux-libre-lts-api-headers' 'dnsmasq' 'lxc'
	     'libiscsi' 'open-iscsi'
	     'perl-xml-xpath' 'libxslt' 'qemu' 'quilt')
optdepends=('ebtables: required for default NAT networking'
	    'dnsmasq: required for default NAT/DHCP for guests'
	    'bridge-utils: for bridged networking'
	    'openbsd-netcat: for remote management over ssh'
	    'qemu'
	    'radvd'
	    'dmidecode')
options=('emptydirs')
backup=('etc/conf.d/libvirt-guests'
	'etc/conf.d/libvirtd'
	'etc/libvirt/libvirt.conf'
	'etc/libvirt/virtlogd.conf'
	'etc/libvirt/libvirtd.conf'
	'etc/libvirt/lxc.conf'
	'etc/libvirt/nwfilter/allow-arp.xml'
	'etc/libvirt/nwfilter/allow-dhcp-server.xml'
	'etc/libvirt/nwfilter/allow-dhcp.xml'
	'etc/libvirt/nwfilter/allow-incoming-ipv4.xml'
	'etc/libvirt/nwfilter/allow-ipv4.xml'
	'etc/libvirt/nwfilter/clean-traffic.xml'
	'etc/libvirt/nwfilter/no-arp-ip-spoofing.xml'
	'etc/libvirt/nwfilter/no-arp-mac-spoofing.xml'
	'etc/libvirt/nwfilter/no-arp-spoofing.xml'
	'etc/libvirt/nwfilter/no-ip-multicast.xml'
	'etc/libvirt/nwfilter/no-ip-spoofing.xml'
	'etc/libvirt/nwfilter/no-mac-broadcast.xml'
	'etc/libvirt/nwfilter/no-mac-spoofing.xml'
	'etc/libvirt/nwfilter/no-other-l2-traffic.xml'
	'etc/libvirt/nwfilter/no-other-rarp-traffic.xml'
	'etc/libvirt/nwfilter/qemu-announce-self-rarp.xml'
	'etc/libvirt/nwfilter/qemu-announce-self.xml'
	'etc/libvirt/qemu-lockd.conf'
	'etc/libvirt/qemu.conf'
	'etc/libvirt/qemu/networks/default.xml'
	'etc/libvirt/virt-login-shell.conf'
	'etc/libvirt/virtlockd.conf'
	'etc/logrotate.d/libvirtd'
	'etc/logrotate.d/libvirtd.lxc'
	'etc/logrotate.d/libvirtd.qemu'
	'etc/logrotate.d/libvirtd.uml'
	'etc/sasl2/libvirt.conf')
install="libvirt.install"
validpgpkeys=('C74415BA7C9C7F78F02E1DC34606B8A5DE95BC1F')
source=("https://libvirt.org/sources/$pkgname-$pkgver.tar.xz"{,.asc}
	"https://deb.debian.org/debian/pool/main/libv/libvirt/libvirt_$_debver-$_debrel.debian.tar.xz"
	"ae102b5d7bccd29bc6015a3e0acefeaa90d097ac.patch::https://libvirt.org/git/?p=libvirt.git;a=patch;h=ae102b5d7bccd29bc6015a3e0acefeaa90d097ac"
	libvirtd.confd
	libvirtd.initd
	libvirtd.modulesd
	virtlockd.initd
	virtlogd.initd
	libvirt-guests.confd
	libvirt-guests.initd
	50-org.libvirt.unix.manage.rules
	fix-mdev-checking-for-VFIO-support.patch)
sha512sums=('35a9db3d59a3817d15fd6a586a2a73860b3d8a2d2ff37dbea1b4045fe1514c3535fce2eeba0f09df2d0bb9072ed9f1078ca9bad61a37c586eb019ddad3f898c4'
            'SKIP'
            '0fe4f61e2defcca22e398a49406095790b2763b469a5711853d4d6258abdbd3be726c0e9e643d8dfd5b4f691985dbc20ca12d37e1706d369079b2d436c471240'
            '915ad9771919e3397702fd8bcf3e9a24d806a8e922826731e2ec49e9390d7f16b1cac849be5e3f44a192f1693ab2e131a75a1574ff8f965f07a882311485b34a'
            '98f935589dbc5f2a99329f77fdf84c563fa0dc99404b7476603679478d68ce8dbea2c88645251d3c28f59e7bcae124ae632972146e8a8c3e0e9fcbfb27296f91'
            'd8e430d1abf117118358474c95cdb7cef4b8e1bd8ed722c11c77873810ed861feb2024ca5c81759d140dcf3618806c1b739b90428ff0592807deffae4c4d0209'
            '871b650ff982be061532b4c5fe4812f41e6e2c58fc69b24f8a745f9a43389da44e742a69b0467b3c3d9e2b031af0728e20f10fa4584695c4f5ac87768a1fd34e'
            '65a05d406200da9a534df5caa1b9cb2af97f0b03f64e6b4157322975fa754f59ae74fff9ec8301a6c0510e4b75e2b72a78cddf004a76109e18c375e83527cee5'
            '0a3e083e742f9e0d8206ab9a48c63bbd395bdc24e5551e2cbeddffa3fb576a817b73dbafe646c392a35c354d6b65426a9b6f3ce3dcafc30077715e90684968c1'
            '78f419a89de7aabaad860903c24f1c070786fc1296d987e61dd537705b0e7f71a46c315888028f3826c3417e42861ca8471788be4ec5695e98427c5f18ae63fc'
            '0f46ec5776c61d1776248779fa894b1a6d7b98d0eb8e8374b320c785c25d92468f087e350233b9152eb2cd0680f6b334c3a61684dabc7c5b559d3b55fdcb712d'
            'fab8e47998e9e9d387050e1d72baa2ea3e2f4ec4687d7c9449ccb95f1d7c4ad0b02fca3da2acab053d1f98ad7f3404138da846b3dc31ab3faf02ce5f3e43bf8e'
            '9a357c77b7afe2095fc983d72f7cd2a60f9f71b27e34d733e7a2f2a52a974bb2ad8046c64369b00f734624eed840d510ef5e80377ec38ee397fc9a66295f82b6')

prepare() {
  cd "$srcdir/$pkgname-$pkgver"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/debian/Debianize-libvirt-guests.patch || true
    rm -v debian/patches/debian/Debianize-systemd-service-files.patch || true
    rm -v debian/patches/debian/Debianize-virtlockd.patch || true
    rm -v debian/patches/debian/Debianize-virtlogd.patch || true
    rm -v debian/patches/debian/fix-Debian-specific-path-to-hvm-loader.patch || true
    rm -v debian/patches/debian/remove-RHism.diff.patch || true

    quilt push -av
  fi

  for file in $(find . -name '*.py' -print); do
    sed -i 's_#!.*/usr/bin/python_#!/usr/bin/python2_' $file
    sed -i 's_#!.*/usr/bin/env.*python_#!/usr/bin/env python2_' $file
  done

  sed -i 's|/sysconfig/|/conf.d/|g' \
    tools/{libvirt-guests.sh,virt-pki-validate}.in
  # 78 is kvm group: https://wiki.archlinux.org/index.php/DeveloperWiki:UID_/_GID_Database
  sed -i 's|#group =.*|group="78"|' src/qemu/qemu.conf
  sed -i 's|/usr/libexec/qemu-bridge-helper|/usr/libexec/qemu/qemu-bridge-helper|g' \
    src/qemu/qemu{.conf,_conf.c} \
    src/qemu/test_libvirtd_qemu.aug.in

  patch -p1 -i "$srcdir"/ae102b5d7bccd29bc6015a3e0acefeaa90d097ac.patch
  patch -p1 -i "$srcdir"/fix-mdev-checking-for-VFIO-support.patch
}

build() {
  cd "$srcdir/$pkgname-$pkgver"

  export PYTHON=`which python2`
  export LDFLAGS=-lX11
  [ -f Makefile ] || ./configure --prefix=/usr --libexec=/usr/libexec/"$pkgname" \
	--with-storage-lvm --without-xen --with-udev --without-hal --disable-static \
	--with-init-script=none \
	--with-qemu-user=nobody --with-qemu-group=nobody \
	--with-netcf --with-interface --with-lxc --with-storage-iscsi
	# --with-audit
  make
}

package() {
  cd "$srcdir/$pkgname-$pkgver"

  make DESTDIR="$pkgdir" install

  install -Dm644 COPYING.LESSER "$pkgdir"/usr/share/licenses/$pkgname/COPYING.LESSER

  chown -R 0:78 "$pkgdir"/var/lib/libvirt/qemu
  chmod 0770 "$pkgdir"/var/lib/libvirt/qemu

  chown 0:102 "$pkgdir"/usr/share/polkit-1/rules.d
  chmod 0750 "$pkgdir"/usr/share/polkit-1/rules.d
  install -m644 "$srcdir"/50-org.libvirt.unix.manage.rules "$pkgdir"/usr/share/polkit-1/rules.d

  rm -rf \
	"$pkgdir"/var/run \
	"$pkgdir"/etc/sysconfig \
	"$pkgdir"/etc/rc.d

  rm -f "$pkgdir"/etc/libvirt/qemu/networks/autostart/default.xml

  for f in libvirtd libvirt-guests; do
    install -Dm644 "$srcdir"/$f.confd "$pkgdir"/etc/conf.d/$f
    install -Dm755 "$srcdir"/$f.initd "$pkgdir"/etc/init.d/$f
  done
  for f in virtlockd virtlogd; do
    install -Dm755 "$srcdir"/$f.initd "$pkgdir"/etc/init.d/$f
  done
  install -Dm644 "$srcdir"/libvirtd.modulesd "$pkgdir"/etc/modules.d/libvirtd
}
