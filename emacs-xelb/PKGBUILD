# Contributor: Jesús E. <heckyel@hyperbola.info>

pkgname=emacs-xelb
_srcname=xelb
pkgver=0.18
pkgrel=3
pkgdesc='X protocol Emacs Lisp Binding'
arch=('any')
url='https://github.com/ch11ng/xelb'
license=('GPL-3')
depends=('emacs')
source=("$pkgname-$pkgver.tar.gz::https://github.com/ch11ng/xelb/archive/$pkgver.tar.gz"
        'xelb-autoloads.el')
sha512sums=('72048e12028811b5beb206102bef27b519bf0524e801f2eec5b3eac8dee785a99dc165e6ecafe14680a5fc0025d7d720db76df5fbbcb9a65070402a29de6ad0b'
            '57a09df3e5a770176b81bce1f808836992b020f6c75da944aef60ebe42ca40ae0e910eea247c9b1845fdfc942e774010e4c6b0be63142e12e2efddcbe02af27e')

prepare() {
  cd "$_srcname-$pkgver"

  # Add autoload
  install -m 0644 "$srcdir/xelb-autoloads.el" .
}

build() {
  cd "$_srcname-$pkgver"

  emacs -q --no-splash -batch -L . -f batch-byte-compile *.el
}

package() {
  cd "$_srcname-$pkgver"

  install -d "$pkgdir/usr/share/emacs/site-lisp/xelb"
  for i in *.el{,c}; do
    install -m644 "$i" "$pkgdir/usr/share/emacs/site-lisp/xelb/$i"
  done

  # license
  install -d "$pkgdir/usr/share/licenses/$pkgname"
  sed -n -e '3,25s/;; //' -e '3,25p' xelb.el > "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}

# vim:set ts=2 sw=2 et:
