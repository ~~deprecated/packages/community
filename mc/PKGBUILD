# $Id: PKGBUILD 215418 2017-03-09 17:51:51Z schuay $
# Maintainer (Arch): schuay <jakob.gruber@gmail.com>
# Contributor (Arch): Daniel J Griffiths <ghost1227@archlinux.us>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

pkgname=mc
pkgver=4.8.19
_debver=4.8.18
_debrel=1
pkgrel=1.hyperbola1
pkgdesc="Midnight Commander is a text based filemanager/shell that emulates Norton Commander, with arj recommendation, without python2-boto recommendation and without nonfree unace and unrar support"
arch=('i686' 'x86_64' 'armv7h')
url="https://midnight-commander.org/"
license=('GPL')
depends=('e2fsprogs' 'glib2' 'gpm' 'libssh2' 'slang')
makedepends=('libxt' 'libx11' 'unzip' 'quilt')
optdepends=('aspell: spelling corrections'
            'cabextract: ucab extfs'
            'cdparanoia: audio extfs'
            'cdrkit: iso9660 extfs'
            'cvs: CVS support'
            'gawk: hp48+ extfs'
            'mtools: a+ extfs'
            'p7zip: support for 7zip archives'
            'perl: needed by several extfs scripts'
            'python2-pytz: s3+ extfs'
            'samba: VFS support'
            'arj: uarj extfs'
            'zip: uzip extfs')
backup=('etc/mc/edit.indent.rc'
        'etc/mc/filehighlight.ini'
        'etc/mc/mc.ext'
        'etc/mc/mc.keymap'
        'etc/mc/mc.menu'
        'etc/mc/mcedit.menu'
        'etc/mc/sfs.ini')
options=('!emptydirs')
source=("http://ftp.midnight-commander.org/${pkgname}-${pkgver}.tar.xz"
        "https://deb.debian.org/debian/pool/main/m/mc/mc_$_debver-$_debrel.debian.tar.xz"
        'libre.patch')
sha512sums=('aec49f0da1fc69099ce9b0f71926d1e9ee6df0be12f53da94a8f5f4266612515031cd02f328a458bf78260c5efb1c8ca2c34c89e20a77567c8b987e43d446531'
            'ff8657148a8fb58f4d80631208249bff09fd331e8cdecf80b5968dd077084f50fbd3f151c4d700b8258d7e9613bc34dfecdf566ffa991dc6298df7a074c4221e'
            '800e9cc1e64ecbec56225f1249f533aec870282b00ea6636176b6c2e3631ccb1f890c7731e774e786b2299bacf83f6238d94191b88a49a7812d6b1a5668b3f1e')

prepare() {
  cd ${pkgname}-${pkgver}

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/uzip_528239.patch || true
    rm -v debian/patches/disable_internal_editor.patch || true
    rm -v debian/patches/mcedit_auto_indent.patch || true
    rm -v debian/patches/mcedit_full_path.patch || true
    rm -v debian/patches/mcedit_group_undo.patch || true

    quilt push -av
  fi

  patch -Np1 -i ../libre.patch
  rm -v src/vfs/extfs/helpers/u{ace,rar}.in
}

build() {
  export PYTHON=/usr/bin/python2

  cd ${pkgname}-${pkgver}
  ./configure \
      --prefix=/usr \
      --libexecdir=/usr/lib \
      --sysconfdir=/etc \
      --enable-vfs-smb \
      --with-x
  make
}

package() {
  cd ${pkgname}-${pkgver}
  make DESTDIR="${pkgdir}" install

  # Replace mc.keymap symlink with target file to fix backup mechanism (FS#50889).
  rm "${pkgdir}"/etc/mc/mc.keymap && cp "${pkgdir}"/etc/mc/mc{.default,}.keymap
}
