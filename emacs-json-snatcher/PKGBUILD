# Contributor: Jesús E. <heckyel@hyperbola.info>

pkgname=emacs-json-snatcher
_srcname=json-snatcher
pkgver=1.0.0
pkgrel=1
pkgdesc='Get the path to a JSON element in Emacs'
arch=('any')
url='https://github.com/Sterlingg/json-snatcher'
license=('GPL-3')
depends=('emacs')
install='emacs-json-snatcher.install'
source=("$pkgname-$pkgver.tar.gz::https://github.com/Sterlingg/json-snatcher/archive/$pkgver.tar.gz")
sha512sums=('ac1e77288561eee5c4c0f5059411ee3d4e507c96bea34c464d1e797d707d4f5c6d214f65dfeb9ffdcfa44b1f8bff47bb917b533f5e5c5d000f65c0a7840ddff8')

build() {
  cd "$_srcname-$pkgver"

  emacs -q --no-splash -batch -L . -f batch-byte-compile json-snatcher.el
}

package() {
  cd "$_srcname-$pkgver"

  install -d "$pkgdir/usr/share/emacs/site-lisp/json-snatcher"
  for i in json-snatcher.el{,c}; do
    install -m644 "$i" "$pkgdir/usr/share/emacs/site-lisp/json-snatcher/$i"
  done

  # doc
  install -d "$pkgdir/usr/share/doc/$pkgname/"
  install -m644 README.md "$pkgdir/usr/share/doc/$pkgname/README.md"

  # license
  install -d "$pkgdir/usr/share/licenses/$pkgname"
  sed -n -e '57,70s/;; //' -e '57,70p' json-snatcher.el > "$pkgdir/usr/share/licenses/$pkgname/COPYING"

}
# vim:set ts=2 sw=2 et:
