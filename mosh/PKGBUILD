# Maintainer (Arch): Alexander F Rødseth <xyproto@archlinux.org>
# Contributor (Arch): Christian Neukirchen <chneukirchen@gmail.com>
# Contributor (Arch): Thomas Weißschuh <thomas_weissschuh lavabit.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=mosh
pkgver=1.3.0
pkgrel=2.hyperbola1
pkgdesc='Mobile shell, surviving disconnects with local echo and line editing'
arch=('i686' 'x86_64')
url='https://mosh.mit.edu/'
license=('GPL-3')
depends=('protobuf' 'ncurses' 'zlib' 'openssh' 'perl' 'perl-io-tty' 'libutempter')
source=("https://$pkgname.mit.edu/$pkgname-$pkgver.tar.gz")
sha512sums=('2b43e3e3fb2ccf6c29a889f10dfc0d5504dbca6fabaf10419f4e355e4b73d64cc1e632324e447b25ac24ee18bb88d8a9a23f9c4824b267343fb86e26e7d5c54b')
options=('!emptydirs')

build() {
  cd "$pkgname-$pkgver"

  ./autogen.sh
  ./configure --prefix=/usr
  make
}

package() {
  make -C "$pkgname-$pkgver" DESTDIR="$pkgdir" install
  install -Dm644 "$pkgname-$pkgver/conf/bash-completion/completions/$pkgname" \
    "$pkgdir/usr/share/bash-completion/completions/$pkgname"
  install -Dm644 "$pkgname-$pkgver/COPYING" \
    "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}

# vim:set ts=2 sw=2 et:
