# Maintainer (Arch): Alexander F. Rødseth <xyproto@archlinux.org>
# Contributor (Arch): Simon Parzer <simon.parzer@gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=grafx2
pkgver=2.6
_debver=2.6
_debrel=2
pkgrel=2.hyperbola1.backports1
_recoil=4.3.1
pkgdesc='Pixelart-oriented painting program'
arch=(i686 x86_64)
url='https://pulkomandy.tk/projects/GrafX2'
license=(GPL-2)
depends=(fontconfig lua sdl2_image sdl2_ttf)
makedepends=(quilt)
options=(!emptydirs)
source=("https://pulkomandy.tk/projects/GrafX2/downloads/grafX2-v$pkgver.tar.bz2"
        "https://deb.debian.org/debian/pool/main/g/grafx2/grafx2_$_debver-$_debrel.debian.tar.xz"
        "https://sourceforge.net/projects/recoil/files/recoil/$_recoil/recoil-$_recoil.tar.gz")
sha512sums=('27c5bd0ec7bac2e354e5e03a9a7a9eea216098166c8155f41dfc5f2663510db85d61701667d4108cd5d1594642a4be88ab7f86cebd67d8cca581eeed940c98c2'
            'c9f3ef32aa479b42ee7620fdaed0d742eb3947c73d89db599cab5fb2426745c82a7c2131b335861e96e3fadc31a9e68a454b432048202462ee1fcbfc69b1628d'
            'd61f982d7ace041d30145da46bcbe75d5f2b10b4d84bb9ef33e5635cff89c0dc4357b1f179f0ad05a08389370cabd93cff0fe5f6655c52e9c16910f2c91bc9d5')

prepare() {
  cd grafX2-v$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  sed 's/-liconv//g' -i src/Makefile
  mkdir -p 3rdparty/archives
  cp -uv ../recoil-$_recoil.tar.gz 3rdparty/archives/recoil-$_recoil.tar.gz
}

build() {
  export CFLAGS="$CFLAGS -w"

  cd grafX2-v$pkgver/src
  make PREFIX=/usr API=sdl2
}

package() {
  export CFLAGS="$CFLAGS -w"

  cd grafX2-v$pkgver/src
  make PREFIX=/usr DESTDIR="$pkgdir" API=sdl2 install

  install -d "$pkgdir/usr/share/grafx2/scripts/samples_2.4/picture/"{thomson/lib,others-8bit/lib}
  mv "$pkgdir/usr/bin/grafx2-sdl2" "$pkgdir/usr/bin/grafx2"
  install -Dm644 ../doc/README.txt "$pkgdir/usr/share/doc/$pkgname/README"
  install -Dm644 ../LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

# vim: ts=2 sw=2 et:
