# Maintainer (Arch):  Andrew O'Neill <andrew at meanjollies dot com>
# Contributor (Arch): Stefan Husmann <stefan-husmann@t-online.de>
# Contributor (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): Eric Belanger <belanger@astro.umontreal.ca>
# Contributor (Arch): Ravi Desai <ravster3@hotmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=xfe
pkgver=1.43.2
_debver=1.43.1
_debrel=1
pkgrel=2.hyperbola1
pkgdesc='X File Explorer (Xfe) is an MS-Explorer like file manager for X, with wget and unar support'
arch=('i686' 'x86_64')
url='http://roland65.free.fr/xfe'
license=('GPL-2')
depends=('fox>=1:1.6' 'fox<1:1.7' 'freetype2' 'xcb-util')
makedepends=('intltool' 'quilt')
source=("https://downloads.sourceforge.net/sourceforge/$pkgname/$pkgname-$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/x/xfe/xfe_$_debver-$_debrel.debian.tar.xz"
        'add-webm-support.patch'
        'libre.patch')
sha512sums=('ad7b450d0828747f801219c23824b0751fc2a7f0eea791369c4adb3090ffe1279056fa8804ef97b25a2f810e00d37357efc962dc9d4087c3d7cc5f8ffb37d51d'
            '8ff4872aa7bb3feaf925036a3be7bed207a5d8f94fb6ba27e81ec0cf1dc34405ab4a09f630b7fa25794a53ed6181598db57c280ad6b73e74f377b9a58f746b96'
            '54874823173b99c5d7421705a5831c21dbdcdb0b8b9e84c8e2865b3fad9a8279f91739c113bc4f9788de95cd0b44dc14384bc246e5f510a2f6006c05bfd55cd1'
            'a0a0dfb31b5bf371b20011eb335f9739c65915fbde8cb50ce999ef258c8744eff0adfa736cfe5bcfec5162e76a80de7175aefabd6870dcaa710d7415176e9895')

prepare() {
  cd $srcdir/$pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian/patches debian

    # Doesn't apply and seems unimportant
    rm -v debian/patches/01_app-names-in-desktop.patch || true
    rm -v debian/patches/02_app-names-in-xfedefs.patch || true
    rm -v debian/patches/07_xferc-initial-for-debian.patch || true
    rm -v debian/patches/10_xournal-support-in-xferc.patch || true
    rm -v debian/patches/11_qlcplus-support-in-xferc.patch || true
    rm -v debian/patches/12_cherrytree-support-in-xferc.patch || true

    quilt push -av
  else
    # add webm support (based on Debian's 13_webm-support-in-xferc.patch)
    patch -p1 -i ../add-webm-support.patch
    # replace nonfree UnRAR with unar (based on Debian's 26_unrar-using-free-unar.patch)
    patch -p1 -i ../libre.patch
  fi
  export CFLAGS="$CFLAGS `pkg-config --cflags freetype2`"
  aclocal
  automake --add-missing
  autoreconf
}

build() {
  cd $srcdir/$pkgname-$pkgver
  ./configure --prefix=/usr
  make
}

package() {
  cd $srcdir/$pkgname-$pkgver
  make DESTDIR=$pkgdir install
  install -Dm644 COPYING $pkgdir/usr/share/licenses/$pkgname/COPYING
}
