# Maintainer (Parabola): Luke Shumaker <lukeshu@sbcglobal.net>
# Maintainer (Arch): Dan McGee <dan@archlinux.org>
# Contributor (Arch): Shahar Weiss <sweiss4@gmx.net>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgbase=django17
pkgname=('python-django17' 'python2-django17')
pkgver=1.7.11
_debver=1.7.11
_debrel=1
pkgrel=1
pkgdesc="A high-level Python Web framework that encourages rapid development and clean design"
arch=('any')
license=('BSD')
url="http://www.djangoproject.com/"
makedepends=('python2' 'python2-setuptools' 'python' 'python-setuptools' 'quilt')
source=("https://www.djangoproject.com/m/releases/${pkgver:0:3}/Django-$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/p/python-django/python-django_$_debver-$_debrel+deb8u2.debian.tar.xz")
sha512sums=('2e3415295ee6b590e366b09adb261baccb750519b9bcef1bb09bd52a2db705c8082bfc13440c621a5f41dd4f9bd626792eba4a572e78de3b7caf93c951c406ee'
            '2b2844d2e7ebcaaacef1db2aa9013d151b40cb3da0c0de1d8b453180465dd0915fc437c1b0c4d82b68939193b241b4a78e23a896a253418b9ce02152e8943baa')

prepare() {
  cd "$srcdir/Django-$pkgver"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/0003-Use-Debian-GeoIP-database-path-as-default.patch || true

    quilt push -av
  fi

  cp -a "$srcdir/Django-$pkgver" "$srcdir/Django-$pkgver-python2"

  find "$srcdir/Django-$pkgver-python2" -name '*.py' | \
    xargs sed -i "s|#!/usr/bin/env python$|#!/usr/bin/env python2|"
}

build() {
  cd "$srcdir/Django-$pkgver"
  python setup.py build

  cd "$srcdir/Django-$pkgver-python2"
  python2 setup.py build
}

package_python-django17() {
  depends=('python' 'python-setuptools')
  optdepends=('python-psycopg2: for PostgreSQL backend')
  provides=("python-django=$pkgver")
  conflicts=('python-django')
  cd "$srcdir/Django-$pkgver"
  python setup.py install --root="$pkgdir" --optimize=1

  ln -s django-admin.py "$pkgdir"/usr/bin/django-admin3.py
  ln -s django-admin "$pkgdir"/usr/bin/django-admin3
  install -Dm644 extras/django_bash_completion \
    "$pkgdir"/usr/share/bash-completion/completions/django-admin.py
  ln -s django-admin.py \
    "$pkgdir"/usr/share/bash-completion/completions/django-admin
  ln -s django-admin.py \
    "$pkgdir"/usr/share/bash-completion/completions/manage.py

  install -Dm644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}

package_python2-django17() {
  depends=('python2' 'python2-setuptools')
  optdepends=('mysql-python: for MySQL backend'
              'python2-psycopg2: for PostgreSQL backend')
  replaces=('django')
  provides=("python2-django=$pkgver")
  conflicts=('django' 'python2-django')
  cd "$srcdir/Django-$pkgver-python2"
  python2 setup.py install --root="$pkgdir" --optimize=1

  mv "$pkgdir"/usr/bin/django-admin.py "$pkgdir"/usr/bin/django-admin2.py
  mv "$pkgdir"/usr/bin/django-admin "$pkgdir"/usr/bin/django-admin2
  # TODO: this probably won't work due to the `complete` command within not
  # knowing about modified our exectuable names
  install -Dm644 extras/django_bash_completion \
    "$pkgdir"/usr/share/bash-completion/completions/django-admin2.py
  ln -s django-admin2.py \
    "$pkgdir"/usr/share/bash-completion/completions/django-admin2

  install -Dm644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}
