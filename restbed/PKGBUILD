# Maintainer (Arch): Baptiste Jonglez <baptiste--aur at jonglez dot org>
# Contributor (Arch): Justin Wilcox <nat1192 at gmail dot com>
# Maintainer: André Silva <emulatorman@hyperbola.info>
pkgname=restbed
pkgver=4.0
_debver=4.0~dfsg1
_debrel=4
pkgrel=1.hyperbola2
pkgdesc="A framework for asynchronous RESTful functionality in C++11 applications, with libressl support"
arch=('i686' 'x86_64')
url="https://github.com/Corvusoft/restbed"
license=('AGPL-3')
depends=('libressl')
replaces=('restbed-latest')
conflicts=('restbed-latest')
makedepends=('cmake' 'asio' 'kashmir' 'quilt')
source=("$pkgname-$pkgver.tar.gz::https://github.com/Corvusoft/restbed/archive/$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/r/restbed/restbed_$_debver-$_debrel.debian.tar.xz")
sha512sums=('4262260c0b740e0be1c42e59e1680ab664b9e5b8f07e354b2bc66a7cdf29d738997011687689b3a25fdf365ddcb7861c8b9b0642053b7f5e956142973abfe6f5'
            '3f8717815667150dafc0f3e12317b74c2bc9b36e0e09e538706ac87ce1a9dce72c385c81a31ba48582f5e8f767dd9c7010b77bf11840220b4b9ee63b0d5e62b2')

prepare() {
  cd "$srcdir/$pkgname-$pkgver"
  if [[ ${pkgver} = ${_debver%~*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "$srcdir/$pkgname-$pkgver"

  mkdir -p build
  cd build
  cmake .. \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DBUILD_SHARED=on \
    -DBUILD_SSL=on
  make
}

package() {
  cd "$srcdir/$pkgname-$pkgver"

  cd build/
  make DESTDIR="$pkgdir" install

  install -Dm644 ../LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}
