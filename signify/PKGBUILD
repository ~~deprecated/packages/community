# Maintainer (Arch): Levente Polyak <anthraxx[at]archlinux[dot]org>
# Contributor (Arch): Adrián Pérez de Castro <aperez@igalia.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=signify
pkgver=26
_debver=26
_debrel=2
pkgrel=1.hyperbola1
pkgdesc='OpenBSD tool to signs and verify signatures on files'
url='https://github.com/aperezdc/signify'
arch=('i686' 'x86_64')
license=('ISC')
depends=('glibc' 'libbsd>=0.8')
makedepends=('quilt')
source=(https://github.com/aperezdc/${pkgname}/releases/download/v${pkgver}/${pkgname}-${pkgver}.tar.xz{,.sig}
        https://deb.debian.org/debian/pool/main/s/signify-openbsd/signify-openbsd_$_debver-$_debrel.debian.tar.xz
        LICENSE)
sha512sums=('1b54f42bb820022672aceb9d05986700e51b6a878d5b1b52df189c43773c027543fa58fda625567ee9e70765a8ee141a30511abbb425d9bf69fd11ed64067cef'
            'SKIP'
            'aaef1d9e359898c631f3f37a3e7a3a43df21b84fc8259b07284a4c8e94b80e01065c5c74e30cdb1199125549d714656f992ff8e588bf5022bd44e7355753575a'
            '0cd25d77effc7e25d37de4aa4a78333fe96e893806c578d0325d6ffa332bd618c9e6100e7fb8a5e87e1a04c67eb5c09effba21dddc81cd808aa8849b6846a18b')
validpgpkeys=('5AA3BC334FD7E3369E7C77B291C559DBE4C9123B') # Adrián Pérez de Castro

prepare() {
  cd ${pkgname}-${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/0001-replace-signify-with-signify-openbsd-in-the-manpage.patch || true

    quilt push -av
  fi
  # using O_NOFOLLOW doesn't lock anything down here, swapping files
  # without symlinks is still equally open to TOCTOU and it doesn't even
  # matter here. lets prefere supporting symlinked target files.
  sed -E 's/\| ?O_NOFOLLOW//g' -i ./*.c
}

build () {
  cd ${pkgname}-${pkgver}
  make PREFIX=/usr \
    LTO=1 \
    EXTRA_CFLAGS="${CFLAGS} ${CPPFLAGS}" \
    EXTRA_LDFLAGS="${LDFLAGS}" \
    GIT_TAG=''
}

package () {
  cd ${pkgname}-${pkgver}
  make PREFIX=/usr \
    DESTDIR="${pkgdir}" \
    GIT_TAG='' install
  install -Dm 644 README.md CHANGELOG.md -t "${pkgdir}/usr/share/doc/${pkgname}"
  install -Dm 644 "${srcdir}/LICENSE" -t "${pkgdir}/usr/share/licenses/${pkgname}"
}

# vim: ts=2 sw=2 et:
