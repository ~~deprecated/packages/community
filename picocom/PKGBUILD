# $Id: PKGBUILD 207104 2017-01-12 23:30:17Z seblu $
# Maintainer (Arch): Sébastien Luttringer
# Contributor (Arch): Jens Adam <jra@byte.cx>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=picocom
pkgver=2.2
_debver=2.2
_debrel=2~bpo9+1
pkgrel=2.hyperbola1
pkgdesc='Minimal dumb-terminal emulation program, very much like minicom, without systemd support'
url='https://github.com/npat-efault/picocom'
license=('GPL2')
arch=('i686' 'x86_64')
makedepends=('quilt')
depends=('glibc')
source=("$pkgname-$pkgver.tar.gz::https://github.com/npat-efault/picocom/archive/$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/p/picocom/picocom_$_debver-$_debrel.debian.tar.xz")
sha512sums=('21495af30b81dfd1b3f4fea1ecf93c3a9252be1ae2ba94c7a2a07ceb75f562bc2ebc6ef6a8ae61f810ff5c121d4ab572a6f8ba75283e2cb28cf9886f834df0fa'
            '5d48c95c358bee921c4fd14bf918bd80588ad57037b736cc9a3bc140aa9207201ff37d22d46efd1dd8179204f73d2d6e96708f9ef6bed2dea3efae9158c98604')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd $pkgname-$pkgver
  make
}

package() {
  cd $pkgname-$pkgver
  install -D -m 755 picocom "$pkgdir/usr/bin/picocom"
  install -D -m 644 picocom.1 "$pkgdir/usr/share/man/man1/picocom.1"
}

# vim:set ts=2 sw=2 et:
