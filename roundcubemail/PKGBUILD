# Maintainer (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=roundcubemail
pkgver=1.2.5
_debver=1.2.3+dfsg.1
_debrel=4+deb9u3
pkgrel=1.hyperbola1
pkgdesc="A PHP web-based mail client, without nonfree Microsoft SQL Server and Oracle Database support"
arch=('any')
url="https://roundcube.net/"
license=('GPL-3')
depends=('php')
makedepends=('php-composer' 'quilt')
optdepends=('python2')
replaces=('roundcubemail-lts')
conflicts=('roundcubemail-lts')
provides=('roundcubemail-lts')
backup=('etc/webapps/roundcubemail/.htaccess'
        'etc/webapps/roundcubemail/apache.conf')
install=roundcubemail.install
options=('!strip' 'emptydirs')
source=(https://github.com/roundcube/roundcubemail/releases/download/$pkgver/roundcubemail-$pkgver-complete.tar.gz
        https://deb.debian.org/debian/pool/main/r/roundcube/roundcube_$_debver-$_debrel.debian.tar.xz
        apache.conf
        libre.patch)
sha512sums=('8248ba7bbcc322b0ddf2db034734dcf470eca6bbfdc696543dc6f6301c64db8de3ad3554b78c746e484ac8b4048933ba32a8468b68977055bcdf373c635a114b'
            '2f8063ea0962a332afd01f2aaa000be7ff2b66db92e052df0f0fb095f598e76a0ed0434e9bf081e680db0109a4408119acc81b27364e3c36983b48eea977f31e'
            '0e80317ae6f040386b0980d0764bc8a9aa5f7cbf028a210a896763cb7a7fea6d91600eda588922a0eb6d519f2ec1e0a2f723bd7ab554c8b1ad4af49a95101f6e'
            'ea5edb5b4ef5e53c4cada3dafabf9d4eda589887659b1e324e1b3f694f32ddb5a07f7a0571a837f2a56786874def9d04678c31d26c2c4dc0fd549230836add12')

prepare() {
  cd $srcdir/roundcubemail-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/correct-magic-path.patch || true
    rm -v debian/patches/correct_install_path.patch || true
    rm -v debian/patches/CVE-2017-6820.patch || true
    rm -v debian/patches/CVE-2017-8114.patch || true
    rm -v debian/patches/dbconfig-common_support.patch || true
    rm -v debian/patches/debianize-config.patch || true
    rm -v debian/patches/debianize_password_plugin.patch || true
    rm -v debian/patches/map-sqlite3-to-sqlite.patch || true
    rm -v debian/patches/Set-INSTALL_PATH-to-var-lib-roundcube-in-bin-cleandb.patch || true
    rm -v debian/patches/too-old-mdb2.patch || true
    rm -v debian/patches/update_script.patch || true
    rm -v debian/patches/use-debian-jquery-ui.patch || true

    quilt push -av
  fi

  sed -i \
    -e "s|RCUBE_INSTALL_PATH . 'temp.*|'/var/cache/roundcubemail';|" \
    -e "s|RCUBE_INSTALL_PATH . 'logs.*|'/var/log/roundcubemail';|" \
    config/defaults.inc.php \
    program/lib/Roundcube/rcube_config.php

  # Remove nonfree Microsoft SQL Server and Oracle Database support
  rm -rv SQL/{mssql{,.initial.sql},oracle{,.initial.sql}}
  rm -v program/lib/Roundcube/rcube_db_{mssql,oracle}.php
  patch -p1 -i ../libre.patch
}

package() {
  mkdir -p $pkgdir/etc/webapps/roundcubemail
  mkdir -p $pkgdir/usr/share/webapps
  mkdir -p $pkgdir/var/log
  cd $pkgdir/usr/share/webapps
  cp -ra $srcdir/roundcubemail-$pkgver roundcubemail
  cd roundcubemail

  mv .htaccess $pkgdir/etc/webapps/roundcubemail/
  ln -s /etc/webapps/roundcubemail/.htaccess .htaccess

  mv config $pkgdir/etc/webapps/roundcubemail/
  ln -s /etc/webapps/roundcubemail/config config

  install -dm0750 $pkgdir/var/{log,cache}/roundcubemail
  install -Dm0644 $srcdir/apache.conf $pkgdir/etc/webapps/roundcubemail/apache.conf
  install -Dm0644 LICENSE $pkgdir/usr/share/licenses/roundcubemail/LICENSE

  rm -rf temp logs debian
  find "$pkgdir" -name .git -exec rm -rf {} \; || true
  find "$pkgdir" -name .pc -exec rm -rf {} \; || true

  sed -i "s|"$srcdir"/roundcubemail-${pkgver/rc/-rc}/vendor/pear-pear.php.net/Console_CommandLine/data|/usr/share/webapps/roundcubemail/vendor/pear-pear.php.net|g" \
    vendor/pear-pear.php.net/Console_CommandLine/Console/CommandLine/XmlParser.php

  sed -i "s|"$srcdir"/roundcubemail-${pkgver/rc/-rc}|/usr/share/webapps/roundcubemail|g" \
    vendor/pear-pear.php.net/Crypt_GPG/bin/crypt-gpg-pinentry \
    vendor/pear-pear.php.net/Crypt_GPG/Crypt/GPG/Engine.php \
    vendor/pear-pear.php.net/Crypt_GPG/Crypt/GPG/PinEntry.php
}
