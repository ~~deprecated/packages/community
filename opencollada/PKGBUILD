# Maintainer: Márcio Silva <coadde@hyperbola.info>

_pkgname=OpenCOLLADA
pkgname=opencollada
pkgver=1.6.45
pkgrel=1.hyperbola2
_debver=0.1.0
_debrev=~20160714.0ec5063+dfsg1
_debrel=2
pkgdesc='Collaborative Design Activity library for 3D applications'
arch=(i686 x86_64)
url=https://github.com/KhronosGroup/$_pkgname
license=(Expat)
depends=(libxml2 pcre)
makedepends=(cmake quilt)
source=($pkgname-$pkgver.tar.gz::https://github.com/KhronosGroup/$_pkgname/archive/v$pkgver.tar.gz
        https://deb.debian.org/debian/pool/main/${pkgname::1}/$pkgname/${pkgname}_$_debver$_debrev-$_debrel.debian.tar.xz)
sha512sums=(43d19b72453fb743aaf76616328394f006f1f665336ef0b5d2b88b35dbfdf7dc8398fd7d4c4285bc6467c2893df1d44f1448f40e60ac0e82fb973d0ce13e2821
            93ede566779ae4d1cf89f4c1efc4f3ca4508ce574ccce4c21ded94a3442b2cf3d4093857979636f1c4ec1d705f0d3b45e608aed2f8e2f8a7c330ce09af710036)

prepare() {
  cd $_pkgname-$pkgver

  # Debian patches
  export QUILT_PATCHES=debian/patches
  export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
  export QUILT_DIFF_ARGS=--no-timestamps

  mv ../debian .

  quilt push -av

  # Disabled, since DAEValidator needs a deprecated package (zlib)
  # and is useless because DAEValidator it's not added as package
  #
  # Bug:
  # /usr/bin/ld: CMakeFiles/DAEValidator.dir/src/XmlDoc.cpp.o: undefined reference to symbol 'inflateEnd'
  # /usr/lib/libz.so.1: error adding symbols: DSO missing from command line
  # collect2: error: ld returned 1 exit status
  #
  # Use system zlib (version 1.2.2 or 1.2.3 - date 2004-10-03 or 2005-06-18)
  # rm -vr Externals/zlib
  # sed -i '\|add_subdirectory[(][$][{]EXTERNAL_LIBRARIES[}][/]zlib[)]|d' CMakeLists.txt
  # sed -i '\|zlib|d'                                                     DAEValidator/CMakeLists.txt
  # sed -i 's|["]zlib[.]h["]|<zlib.h>|'                                   DAEValidator/src/XmlDoc.cpp

  # Remove DAEValidator code, it's unused code and depends a deprecated (and embedded) zlib
  sed -i '\|DAE[ ]validator[ ]app|d
          \|add_subdirectory[(]DAEValidator[)]|d
          \|add_subdirectory[(][$][{]EXTERNAL_LIBRARIES[}][/]zlib[)]|d
         ' CMakeLists.txt
  rm -vr DAEValidator Externals/zlib

  # Remove COLLADAValidator code, it's unused code
  sed -i '\|COLLADAValidator|d
         ' CMakeLists.txt
  rm -vr COLLADAValidator

  # Remove embedded dependencies
  rm -vr Externals/{Cg,expat,lib3ds,LibXML,MayaDataModel,pcre,zziplib}
}

build() {
  cd $_pkgname-$pkgver

  [ -d build ] && rm -vr build
  mkdir build && cd build

  cmake .. \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_SKIP_RPATH=ON \
    -DUSE_SHARED=ON \
    -DUSE_STATIC=OFF
  make
}

package() {
  cd $_pkgname-$pkgver/build

  make DESTDIR=$pkgdir install

  install -vdm  0755 $pkgdir/etc/ld.so.conf.d
  echo /usr/lib/$pkgname > $pkgdir/etc/ld.so.conf.d/$pkgname.conf
}

# vim:set ts=2 sw=2 et:
