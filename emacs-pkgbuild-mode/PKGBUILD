# Contributor: Jesús E. <heckyel@hyperbola.info>

pkgname=emacs-pkgbuild-mode
_srcname=pkgbuild-mode
pkgver=0.14
pkgrel=1.backports1
pkgdesc='Major mode for editing PKGBUILD files'
arch=('any')
url='https://github.com/juergenhoetzel/pkgbuild-mode'
license=('GPL-2')
depends=('emacs')
install='emacs-pkgbuild-mode.install'
source=("$pkgname-$pkgver.tar.gz::https://github.com/juergenhoetzel/pkgbuild-mode/archive/$pkgver.tar.gz"
        '0001-backport.patch')
sha512sums=('3e2de10e7bef34131bc56e4bfef8b1ef84615dfea1c9a33ff249868c71fe8e66524b306830b2512770010581ead8ab6ba842e7dd27a49394441d305737e2006a'
            '8a1c4836d5999acfd3bc8b9746f5622a07168268b012d76bbc096e538af348ec6cbc0d68598773f8834be90903ccbc5625123342cc95663b12142497f52c6330')

prepare() {
  cd "$_srcname-$pkgver"

  patch -p1 -i "${srcdir}/0001-backport.patch"
}

build() {
  cd "$_srcname-$pkgver"

  emacs -q --no-splash -batch -L . -f batch-byte-compile pkgbuild-mode.el
}

package() {
  cd "$_srcname-$pkgver"

  install -d "$pkgdir/usr/share/emacs/site-lisp/pkgbuild-mode"
  for i in pkgbuild-mode.el{,c}; do
    install -m644 "$i" "$pkgdir/usr/share/emacs/site-lisp/pkgbuild-mode/$i"
  done

  # license
  install -d "$pkgdir/usr/share/licenses/$pkgname"
  sed -n -e '3,23s/;; //' -e '3,23p' pkgbuild-mode.el > "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}

# vim:set ts=2 sw=2 et:
