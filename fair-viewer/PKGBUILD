# Maintainer: André Silva <emulatorman@hyperbola.info>
# Maintainer: Jesús E. <heckyel@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

pkgbase=fair-viewer
pkgname=(fair-viewer gtk-fair-viewer)
epoch=1
pkgver=1.0.6
pkgrel=1
arch=('any')
url='https://framagit.org/heckyel/fair-viewer'
license=('GPL-3')
source=("https://libregit.spks.xyz/heckyel/fair-viewer/raw/branch/archive/$pkgver/fair-viewer-$pkgver.tar.gz"{,.sig})
sha512sums=('266c6dc4b1e83c27d483965555fc0e0be1ec1de13c48e4e796e26b4abd134f0492456bbad3f4c94efe15bf93146bd271620c7df102dc3ef5f0587b19e761438a'
            'SKIP')
validpgpkeys=('4DF21B6A7C1021B25C360914F6EE7BC59A315766') # Jesús E.
depends=('perl>=5.16.0' 'perl-data-dump' 'perl-json' 'perl-lwp-protocol-https' 'perl-libwww')
makedepends=('perl-module-build')
optdepends=(
  'perl-json-xs: faster JSON to HASH conversion'
  'perl-text-charwidth: to print the results in a fixed-width format (-W)'
  'perl-term-readline-gnu: for better STDIN support'
  'hypervideo: for playing videos with hypervideo'
  'wget: for downloading videos with wget'
  'vlc: for playing videos with VLC'
  'mpv: for playing videos with MPV (recommended)'
)

build() {
  cd $pkgbase-$pkgver
  perl Build.PL --installdirs vendor --gtk3
}

check(){
  cd $pkgbase-$pkgver
  ./Build test
}

package_fair-viewer() {
  pkgdesc='Utility for viewing YouTube videos (CLI), with Invidious API and hypervideo support'
  optdepends+=('gtk-fair-viewer: to use fair-viewer with GTK+ GUI')
  replaces=('youtube-viewer')
  conflicts=('youtube-viewer')
  provides=('youtube-viewer')

  cd $pkgbase-$pkgver
  ./Build install --destdir "$pkgdir"
  rm -r "$pkgdir"/usr/lib
  rm "$pkgdir"/usr/bin/vendor_perl/gtk-fair-viewer
  # license
  install -d "$pkgdir/usr/share/licenses/$pkgname"
  install -m644 LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"

  ln -sv fair-viewer "$pkgdir/usr/bin/vendor_perl/youtube-viewer"
}

package_gtk-fair-viewer() {
  pkgdesc='Utility for viewing YouTube videos (GTK+ GUI), with Invidious API and hypervideo support'
  depends+=('fair-viewer' 'perl-gtk3' 'perl-file-sharedir')
  replaces+=('gtk-youtube-viewer')
  conflicts+=('gtk-youtube-viewer')
  provides+=('gtk-youtube-viewer')

  cd $pkgbase-$pkgver
  install -d "$pkgdir/usr/"{bin/vendor_perl,share/{applications,pixmaps}}
  install -Dm 755 "bin/$pkgname" "$pkgdir/usr/bin/vendor_perl/$pkgname"
  install -Dm 644 "share/$pkgname.desktop" "$pkgdir/usr/share/applications/$pkgname.desktop"
  install -Dm 644 "share/icons/$pkgname.png" "$pkgdir/usr/share/pixmaps/$pkgname.png"

  ln -sv gtk-fair-viewer "$pkgdir/usr/bin/vendor_perl/gtk-youtube-viewer"
}
