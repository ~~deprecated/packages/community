# Maintainer (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Maintainer (Arch): Douglas Soares de Andrade <dsa@aur.archlinux.org>
# Contributor (Arch): Roberto Alsina <ralsina@kde.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=pound
pkgver=2.7
_debver=2.7
_debrel=1.3
pkgrel=4.hyperbola1
pkgdesc="A reverse proxy, load balancer, and SSL wrapper, with libressl and OpenRC support"
arch=('i686' 'x86_64')
url="http://www.apsis.ch/pound/index_html"
license=('custom:GPL-3+OpenSSL-Linking-Exception')
depends=('pcre' 'libressl')
makedepends=('quilt')
backup=(etc/pound/pound.cfg)
source=(http://www.apsis.ch/pound/Pound-$pkgver.tgz
        https://deb.debian.org/debian/pool/main/p/pound/pound_$_debver-$_debrel.debian.tar.xz
        pound.initd
        pound.cfg)
sha512sums=('3edfb948715ff7e38b7665cc083eb5619daf4c2ddda4efcffffa2bcf5d8c1ee158fa5275b16bbaea4cf51bf3f8c022ba2197d5f67d57732404a448ae00e62aa1'
            '6978a39b19abc1300ba88625ec03a8769172a5214294d8637fcae2bee136d4d4e651330925bcba84813a359ccfd0cb10b0bd70e79302b830313fbbc1c8042938'
            '2c7615a0606408ce1563b880d439d4b0661b0b3144b442e215be8bb960f2629f169863cf79fe55981c4f9970729667b8478d9f61f227974f211655de5d01d64b'
            '123f3cc5c7264a9908082ef9bec583333c4062c072487309b2f0825d3fe27d110ba50cdab95c31207ed27bbf1e5ce63f4fc3e13178027352d6aae63989900421')

prepare() {
  cd "$srcdir"/Pound-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "$srcdir"/Pound-$pkgver
  ./configure --prefix=/usr --sysconfdir=/etc/pound
  make
}

package() {
  cd "$srcdir"/Pound-$pkgver
  make DESTDIR="$pkgdir" install
  for i in GPL.txt README; do
    install -D -m0644 $i "$pkgdir"/usr/share/licenses/pound/$i
  done
  install -D -m0644 "$srcdir"/pound.cfg "$pkgdir"/etc/pound/pound.cfg
  install -D -m0755 "$srcdir"/pound.initd "$pkgdir"/etc/init.d/pound
}
