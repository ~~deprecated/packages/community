# Maintainer (Arch): Giovanni Scafora <giovanni@archlinux.org>
# Contributor (Arch): BlackEagle < ike DOT devolder AT gmail DOT com >
# Contributor (Arch): Preecha Patumchareonpol <yumyai at gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=podofo
pkgver=0.9.6
_debver=0.9.6+dfsg
_debrel=4
pkgrel=2.hyperbola1
pkgdesc="A C++ library to work with the PDF file format, with libressl support"
arch=('i686' 'x86_64')
url="http://podofo.sourceforge.net"
license=('custom:GPL-2+OpenSSL-Linking-Exception' 'LGPL-2')
depends=('lua' 'libressl' 'fontconfig' 'libtiff' 'libidn' 'libjpeg-turbo')
makedepends=('cmake' 'quilt')
source=("https://downloads.sourceforge.net/${pkgname}/${pkgname}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/libp/libpodofo/libpodofo_$_debver-$_debrel.debian.tar.xz")
sha512sums=('35c1a457758768bdadc93632385f6b9214824fead279f1b85420443fb2135837cefca9ced476df0d47066f060e9150e12fcd40f60fa1606b177da433feb20130'
            '545b76c78aab15f72d66f4652cf1fa4addb6a723f097620f24af3d59c7e34985e24846d899552bbf748ee09f4e7f5866eab07cd5816a795b72453b9e0b13b1c1')

prepare() {
  cd ${pkgname}-${pkgver}

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  # Fix to compile with LibreSSL
  # https://bugs.gentoo.org/663602
  sed -e 's:^#ifdef PODOFO_HAVE_OPENSSL_1_1$:#ifndef PODOFO_HAVE_OPENSSL_1_1:' -i tools/podofosign/podofosign.cpp
}

build() {
  cd ${pkgname}-${pkgver}

  mkdir -p "test/TokenizerTest/objects"

  mkdir build
  cd build
  cmake -DCMAKE_INSTALL_PREFIX=/usr .. \
        -DFREETYPE_INCLUDE_DIR=/usr/include/freetype2 \
        -DPODOFO_BUILD_SHARED=1 \
        -DPODOFO_HAVE_JPEG_LIB=1 \
        -DPODOFO_HAVE_PNG_LIB=1 \
        -DPODOFO_HAVE_TIFF_LIB=1
  make
}

package() {
  cd ${pkgname}-${pkgver}/build
  make DESTDIR=$pkgdir install

  install -d -m755 ${pkgdir}/usr/share/licenses/${pkgname}
  install -m644 ../COPYING* ${pkgdir}/usr/share/licenses/${pkgname}
}
