# Maintainer (Arch): Anatol Pomozov <anatol.pomozov@gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=tarantool
pkgver=1.7.5
_debrev=46.gd98815384
pkgrel=1.hyperbola4
pkgdesc='Lua application server integrated with a database management system, with libressl and OpenRC support'
arch=(i686 x86_64)
url='https://www.tarantool.org'
license=(Simplified-BSD)
depends=(readline ncurses zlib libyaml libressl libunwind)
makedepends=(cmake)
checkdepends=(python2-daemon python2-gevent python2-tarantool)
install=tarantool.install
source=(https://deb.debian.org/debian/pool/main/t/tarantool/tarantool_$pkgver.$_debrev.orig.tar.gz
        tarantool.initd)
sha512sums=('4e941fe6acda7c75d1e3334adb531c5158d9f47dd19fde7f86a9345059625d62189608730840810fa65a8ce95da2e6ac7d1cccb925882048eecfa32b1f6a606a'
            'fe463c4b6ab8708ea6ec17bb6a887677ae131a489d2ce299ce951790b7c134ff356bc1a4d4745878beec3600ec702944c2329a3f02645f8ab0a0eb24edb6215a')

build() {
  cd tarantool_$pkgver.$_debrev

  cmake \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_INSTALL_BINDIR=/usr/bin \
    -DCMAKE_INSTALL_SYSCONFDIR=/etc \
    -DCMAKE_INSTALL_LIBDIR=/usr/lib \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_SKIP_RPATH=ON \
    -DENABLE_BUNDLED_LIBYAML:BOOL=OFF \
    -DENABLE_BACKTRACE:BOOL=ON \
    -DWITH_SYSVINIT:BOOL=OFF \
    -DWITH_SYSTEMD:BOOL=OFF \
    -DENABLE_DIST:BOOL=ON
  make
}

check() {
  cd tarantool_$pkgver.$_debrev/test
  ./test-run.py --suite box
}

package() {
  cd tarantool_$pkgver.$_debrev
  make install DESTDIR="$pkgdir"

  install -Dm644 LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"

  install -Dm755 "$srcdir/tarantool.initd" "$pkgdir/etc/init.d/tarantool"
}
