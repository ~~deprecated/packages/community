# Maintainer (Arch): Gaetan Bisson <bisson@archlinux.org>
# Contributor (Arch): Miguel Revilla <yo@miguelrevilla.com>
# Contributor (Arch): Massimiliano Torromeo <massimiliano.torromeo@gmail.com>
# Contributor (Arch): goodmen <goodmenzy@gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=openipmi
_pkgname=OpenIPMI
pkgver=2.0.22
_debver=2.0.22
_debrel=1.1
pkgrel=2.hyperbola1
pkgdesc='Full-function IPMI (Intelligent Platform Management Interface) system'
url='http://openipmi.sourceforge.net/'
arch=('i686' 'x86_64')
license=('LGPL-2.1')
makedepends=('swig' 'quilt')
depends=('popt' 'ncurses' 'net-snmp' 'glib2' 'gdbm')
source=("https://downloads.sourceforge.net/project/${pkgname}/${_pkgname}%202.0%20Library/${_pkgname}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/o/openipmi/openipmi_$_debver-$_debrel.debian.tar.xz"
        "0001-Add-openssl-1.1.0-support.patch")
sha512sums=('78017fc0b52ce6a83db45f595f50170410120c9f7394429869c177ee40ea5b924bcc0e36bb60f9368a75a2f78ff71c70ae3631ba55a207cfb6eb4f62aa44ff89'
            '3a04f6a12acca599ce55f2b928ed73be5741601df089affb383f67d9c6f86d402f0a4dd5c46a39ccdf583beb924b3a76c9c17773721a181ea0827d78deebc9c8'
            'a121bf26112a257a27bcd4c994f0d6a967643c1b4cffbe53fc11b1d1d8bc8f35dbf5eef664bc8f49c8dda04a0b0c876e6734b51b62af34e9c74e8ada98a9a806')

options=('!libtool')

prepare() {
	cd "${srcdir}/${_pkgname}-${pkgver}"
	if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
		# Debian patches
		export QUILT_PATCHES=debian/patches
		export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
		export QUILT_DIFF_ARGS='--no-timestamps'

		mv "$srcdir"/debian .

		quilt push -av
	else
		patch -Np1 -i $srcdir/0001-Add-openssl-1.1.0-support.patch
	fi

	sed \
		-e '/Requires:/s/pthread//' \
		-e '/Libs:/s/$/ -lpthread/' \
		-i OpenIPMIpthread.pc.in
}

build() {
	cd "${srcdir}/${_pkgname}-${pkgver}"
	./configure \
		--prefix=/usr \
		--sysconfdir=/etc \

	make
}

package() {
	cd "${srcdir}/${_pkgname}-${pkgver}"
	make DESTDIR="${pkgdir}" install-exec
	make DESTDIR="${pkgdir}" install
	install -Dm644 COPYING.LIB "${pkgdir}/usr/share/licenses/${pkgname}/COPYING.LIB"
	install -Dm644 doc/IPMI.pdf "${pkgdir}/usr/share/doc/${pkgname}/IPMI.pdf"
}
