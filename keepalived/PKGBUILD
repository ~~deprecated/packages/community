# Maintainer (Arch): Sébastien Luttringer
# Contributor (Arch): Andrea Zucchelli <zukka77@gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=keepalived
pkgver=1.3.5
_debver=1.3.2
_debrel=1
pkgrel=4.hyperbola6
pkgdesc='Failover and monitoring daemon for LVS clusters, with logger recommendation and with libressl and OpenRC support'
arch=('i686' 'x86_64')
url='http://www.keepalived.org/'
license=('GPL-2')
backup=('etc/keepalived/keepalived.conf' 'etc/sysconfig/keepalived' 'etc/conf.d/keepalived')
depends=('libressl' 'popt' 'libnl1' 'ipset' 'iptables')
optdepends=('ipset: ipset support'
            'logger: message logging support')
makedepends=('libnfnetlink' 'ipset' 'quilt')
options=('!emptydirs')
source=("http://www.keepalived.org/software/$pkgname-$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/k/keepalived/keepalived_$_debver-$_debrel.debian.tar.xz"
        'keepalived.confd'
        'keepalived.initd')
sha512sums=('ebd710167becd4b99b665877953d19477c11f1d1c736eb580a05dedd4946363b756553e20607f34aa6c35f57b7252d8a22287beedc316b62a745fccc4243a720'
            'e61d5473165ded5ff0886e9187c3bfb11c5dca6e2708ee3d51eac7cfdf3270ab7f2b9433b63164a867a2c7df6f8ab121260122294d0bf3ec4bb9446190ad5fc9'
            '468d176d22f1ce308871829f24624b33e6ba3b234a413395599a67a865f3cf815cc24a10c244dffd1421a0acf5352fe51027a2d3b282cd3822dfeadc8a1d4df0'
            '240201b81bcca7b213bd3ec45fc286f0305d72a6952a3baf485d717df413df3a2da0f4b018eaa050b0b12cda78767f5c4ae60ec75e5802cfe57cb29951e86ba0')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    for f in $(ls -1 debian/patches); do
      echo $f >> debian/patches/series
      sed -i '\|series|d' debian/patches/series
    done

    # Doesn't apply and seems unimportant
    rm -v debian/patches/fix_manpage_errors || true
    rm -v debian/patches/libnl3.patch || true

    quilt push -av
  fi
}

build() {
  cd $pkgname-$pkgver
  ./configure --prefix=/usr --sysconfdir=/etc
  make
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install
  # license
  install -D -m 644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
  # move examples to /usr/share
  install -d -m 755 "$pkgdir/usr/share/$pkgname"
  mv "$pkgdir/etc/keepalived/samples" "$pkgdir/usr/share/$pkgname/samples"
  # OpenRC
  install -D -m 644 "$srcdir/keepalived.confd" "$pkgdir/etc/conf.d/keepalived"
  install -D -m 755 "$srcdir/keepalived.initd" "$pkgdir/etc/init.d/keepalived"
}

# vim:set ts=2 sw=2 et:
