# Maintainer (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=opensips
pkgver=2.2.3
_debver=2.2.2
_debrel=3
pkgrel=1.hyperbola3
pkgdesc="A Free Software SIP Server able to act as a SIP proxy, registrar, location server, redirect server, with libressl and OpenRC support"
url="https://www.opensips.org"
depends=('gcc-libs' 'libressl' 'db' 'attr' 'libxml2')
makedepends=('postgresql-libs>=8.4.1' 'unixodbc' 'libldap>=2.4.18' 'libmariadbclient'
             'lynx' 'libxslt' 'libmicrohttpd' 'quilt')
optdepends=('postgresql-libs'
            'unixodbc'
            'libldap'
            'libmariadbclient'
            'libsasl'
            'python2'
            'pcre')
backup=("etc/opensips/opensips.cfg"
        "etc/opensips/osipsconsolerc"
        "etc/opensips/opensipsctlrc"
        "etc/conf.d/opensips")
arch=('i686' 'x86_64')
license=('custom:GPL-2+OpenSSL-Linking-Exception')
install=opensips.install
options=('!emptydirs' 'zipman' '!makeflags' 'docs')
source=(https://opensips.org/pub/opensips/${pkgver}/opensips-${pkgver}.tar.gz
        https://deb.debian.org/debian/pool/main/o/opensips/opensips_$_debver-$_debrel.debian.tar.xz
        opensips.confd
        opensips.initd
        port-tls-1.1.0.patch)
sha512sums=('42759e0493368311176f9a5371cf4301db8ee44edc8babd1a6d173afa58feba13e69517e98e232486466683a3ebc9bca473c488b6ae9a54920fd29f44a4fadc3'
            'e2b5adad2d172375610c0012ffe8f79c4ecc82850e219f5a7380dbcdac873326b0d1b41f768b44c5de04a788af1dceb2f2a20d5c7b0a45704edf0715ca72891c'
            '791198b9dfe57d2b561387c80d23e04ebf09b9ab80eb263fcaeaf77cc2d211a1596665beafaed1b689f318c7105e4a4199a3a450cc949d5cea5459a0ab9e924c'
            '63d035bf03864debeb3a14823417c83947d88c16e39edf5a8a7f906a2576f8a8c9e331eba6cc65af7b89a3e0b2b9df9d9799e18a55a976d6dd0e0a2a377428ab'
            '394b6f5c061c8abde70e4c5b53479bf54f635a4cf786bcabf2680dbb2f3af0328e09a81179050fddf98ba09b1fbcde57a93e8d3a77bfc9079096b32f2e6785cd')

prepare() {
  cd "$srcdir"/$pkgname-$pkgver/

  if [[ $pkgver = $_debver ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/fix-radcli.patch || true

    quilt push -av
  else
    patch -Np1 -i ../port-tls-1.1.0.patch
  fi

  # python2 fix
  for file in $(find . -name '*.py' -print); do
    sed -i 's_^#!.*/usr/bin/python_#!/usr/bin/python2_' $file
    sed -i 's_^#!.*/usr/bin/env.*python_#!/usr/bin/env python2_' $file
  done
}

_modules="ldap db_mysql db_postgres db_unixodbc presence presence_xml h350 proto_tls tlsops tls_mgm db_http httpd tm rr"

build() {
  cd "$srcdir"/$pkgname-$pkgver/

  make \
	include_modules="${_modules}" \
	LIBDIR=lib PREFIX=/usr
}

package() {
  cd "$srcdir"/$pkgname-$pkgver/

  make \
	include_modules="${_modules}" \
	BASEDIR="$pkgdir" PREFIX=/usr LIBDIR=lib install

  install -Dm644 COPYING "$pkgdir"/usr/share/licenses/opensips/COPYING

  mkdir -p "$pkgdir"/etc/
  mv "$pkgdir"/usr/etc/opensips/ "$pkgdir"/etc/
  sed -i 's#mpath=".*lib/opensips/modules/"#mpath="/usr/lib/opensips/modules/"#' "$pkgdir"/etc/opensips/opensips.cfg

  # fix bad paths
  cd "$pkgdir"/usr/share
  find -type f -exec sed -i "s#"$pkgdir"##" {} \;

  cd "$pkgdir"/usr/lib/opensips/opensipsctl
  find -type f -exec sed -i "s#"$pkgdir"##" {} \;

  cd "$pkgdir"/usr/sbin
  sed -i "s#"$pkgdir"##" opensipsctl opensipsdbctl osipsconsole

  cd "$pkgdir"/etc
  find -type f -exec sed -i "s#"$pkgdir"##" {} \;

  install -Dm0644 "$srcdir"/$pkgname.confd "$pkgdir"/etc/conf.d/$pkgname
  install -Dm0755 "$srcdir"/$pkgname.initd "$pkgdir"/etc/init.d/$pkgname
}
